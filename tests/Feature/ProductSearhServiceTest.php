<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use VinciarelliFranco\ProductService\Domain\Product;
use VinciarelliFranco\ProductService\Domain\Category;
use VinciarelliFranco\ProductService\Domain\ProductContract;
use VinciarelliFranco\ProductService\Domain\ProductRating;

class ProductSearhServiceTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();
        $this->prepareScenario();
    }

    /**
     * Test Product search endpoint is reachable.
     *
     * @return void
     */
    public function test_default_products_service_returns_a_successful_response()
    {
        $response = $this->get('/api/v1/products');
        $response->assertStatus(200);        
    }

    /**
     * Test Product search order filters endpoint is reachable.
     *
     * @return void
     */
    public function test_products_order_filter_service_returns_a_successful_response()
    {
        $response = $this->get('/api/v1/products/filters');
        $response->assertStatus(200);        
    }

    /**
     * Test contract type's endpoint is reachable.
     *
     * @return void
     */
    public function test_contracts_types_products_service_returns_a_successful_response()
    {
        $response = $this->get('/api/v1/products/contracts');
        $response->assertStatus(200);        
    }

    /**
     * Test contract type's endpoint is reachable.
     *
     * @return void
     */
    public function test_price_range_products_service_returns_a_successful_response()
    {
        $response = $this->get('/api/v1/products/price-range');
        $response->assertStatus(200);        
    }

    /**
     * Test default call using Product Category filter at URI 
     *
     * @return void
     */
    public function test_price_filter()
    {
    
        $response = $this->get('/api/v1/products?price_from=1499&price_to=1501');
        $response->assertStatus(200);

        $data = json_decode($response->getContent(), true);
        $this->assertEquals(5, Product::count());
        
        //there are 2 product que price set to 1500 having contract type = 12
        $this->assertCount(2, $data['data'],'Categporia 1');
    }

    /**
     * Test default call using Product Contract_type filter at URI 
     *
     * @return void
     */
    public function test_contract_type_filter()
    {
        //Contract 12
        $response = $this->get('/api/v1/products?contract_type=24');
        $response->assertStatus(200);

        $data = json_decode($response->getContent(), true);
        $this->assertEquals(5, Product::count());
        
        //3 Product has Contract for 12 months
        $this->assertCount(3, $data['data']);

        //Contract 24
        $response = $this->get('/api/v1/products?contract_type=24');
        $response->assertStatus(200);

        $data = json_decode($response->getContent(), true);
        $this->assertEquals(5, Product::count());
        
        //3 Product has Contract for 24 months
        $this->assertCount(3, $data['data']);

        //Contract 36
        $response = $this->get('/api/v1/products?contract_type=24');
        $response->assertStatus(200);

        $data = json_decode($response->getContent(), true);
        $this->assertEquals(5, Product::count());
        
        //3 Product has Contract for 36 months
        $this->assertCount(3, $data['data']);

        //Contract 48
        $response = $this->get('/api/v1/products?contract_type=24');
        $response->assertStatus(200);

        $data = json_decode($response->getContent(), true);
        $this->assertEquals(5, Product::count());
        
        //3 Product has Contract for 48 months
        $this->assertCount(3, $data['data']);
    }

    /**
     * Test default call using Product Category filter at URI 
     *
     * @return void
     */
    public function test_category_filter()
    {
        //With Category 1
        $response = $this->get('/api/v1/products?category=1');
        $response->assertStatus(200);

        $data = json_decode($response->getContent(), true);
        $this->assertEquals(5, Product::count());
        
        //2 Product has category = 1 and default contract type of 12
        $this->assertCount(1, $data['data'],'Categporia 1');

        //With Category 2
        $response = $this->get('/api/v1/products?category=2');
        $response->assertStatus(200);

        $data = json_decode($response->getContent(), true);
        $this->assertEquals(5, Product::count());
        
        //2 Product has category = 2 and default contract type of 12
        $this->assertCount(2, $data['data']);
    }

    /**
     * Test default call without filters at URI 
     *
     * @return void
     */
    public function test_default_products_service_returns_3_products()
    {
        $response = $this->get('/api/v1/products');

        $response->assertStatus(200);

        $data = json_decode($response->getContent(), true);

        $this->assertEquals(5, Product::count());
        
        //3 Product has default contract type 12
        $this->assertCount(3, $data['data']);
    }

    private function prepareScenario()
    {
        //2 Categories and 5 Products
        //3 Product with contract type=12
        //3 Product with contract type=24
        //3 Product with contract type=36
        //3 Product with contract type=48

        $cat1 = Category::factory()->make();
        $cat1->id = 1;
        $cat1->save();

        $cat2 = Category::factory()->make();
        $cat2->id = 2;
        $cat2->save();

        //Cat1 have 2 Products
        
        //Product 1
        //P1 have a have contract type for 12 and 24 months

        $p1 = Product::factory()->make();
        $p1->title="Product 1";
        $p1->category_id = $cat1->id;
        $p1->save();
        
        $c1 = ProductContract::factory()->make();
        $c1->months = 12;
        $c1->price = 1500;
        $p1->contracts()->save($c1);

        $c2 = ProductContract::factory()->make();
        $c2->months = 24;
        $p1->contracts()->save($c2);

        $r1 = ProductRating::factory()->make();
        $r1->rate = 1;
        $p1->ratings()->save($r1);

        $r2 = ProductRating::factory()->make();
        $r2->rate = 1;
        $p1->ratings()->save($r2);
        
        //Product 2
        //P2 have a have contract type for 24 and 36 months

        $p2 = Product::factory()->make();
        $p2->title="Product 2";
        $p2->category_id = $cat1->id;
        $p2->save();
        
        $c3 = ProductContract::factory()->make();
        $c3->months = 24;
        $p2->contracts()->save($c3);

        $c4 = ProductContract::factory()->make();
        $c4->months = 36;
        $p2->contracts()->save($c4);

        $r3 = ProductRating::factory()->make();
        $r3->rate = 2;
        $p2->ratings()->save($r3);
       
        $r4 = ProductRating::factory()->make();
        $r4->rate = 2;
        $p2->ratings()->save($r4);

        
        //Cat2 have 3 Products

        //Product 3
        //P3 have contract type for 36 and 48 months
        $p3 = Product::factory()->make();
        $p3->title="Product 3";
        $p3->category_id = $cat2->id;
        $p3->save();
        
        $c5 = ProductContract::factory()->make();
        $c5->months = 36;
        $p3->contracts()->save($c5);

        $c6 = ProductContract::factory()->make();
        $c6->months = 48;
        $p3->contracts()->save($c6);

        $r5 = ProductRating::factory()->make();
        $r5->rate = 3;
        $p3->ratings()->save($r5);
       
        $r6 = ProductRating::factory()->make();
        $r6->rate = 3;
        $p3->ratings()->save($r6);
        
        //Product 4
        //P4 have contract type for 12, 24, 36 and 48 months
        $p4 = Product::factory()->make();
        $p4->title="Product 4";
        $p4->category_id = $cat2->id;
        $p4->save();
        
        $c5 = ProductContract::factory()->make();
        $c5->months = 12;
        $c5->price = 1500;
        $p4->contracts()->save($c5);

        $c6 = ProductContract::factory()->make();
        $c6->months = 24;
        $p4->contracts()->save($c6);

        $c7 = ProductContract::factory()->make();
        $c7->months = 36;
        $p4->contracts()->save($c7);

        $c8 = ProductContract::factory()->make();
        $c8->months = 48;
        $p4->contracts()->save($c8);

        $r7 = ProductRating::factory()->make();
        $r7->rate = 4;
        $p4->ratings()->save($r7);
       
        $r8 = ProductRating::factory()->make();
        $r8->rate = 4;
        $p4->ratings()->save($r8);


        //Product 5
        //P5 have contract type for 12 and 48 months

        $p5 = Product::factory()->make();
        $p5->title="Product 5";
        $p5->category_id = $cat2->id;
        $p5->save();
        
        $c9 = ProductContract::factory()->make();
        $c9->months = 12;
        $p5->contracts()->save($c9);
        
        $c10 = ProductContract::factory()->make();
        $c10->months = 48;
        $p5->contracts()->save($c10);

        $r9 = ProductRating::factory()->make();
        $r9->rate = 4;
        $p5->ratings()->save($r9);
       
        $r10 = ProductRating::factory()->make();
        $r10->rate = 4;
        $p5->ratings()->save($r10);
    
    }
}
