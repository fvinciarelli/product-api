<?php declare(strict_types = 1);

return [
	'lastFullAnalysisTime' => 1670377685,
	'meta' => array (
  'cacheVersion' => 'v10-collectedData',
  'phpstanVersion' => '1.9.2',
  'phpVersion' => 80026,
  'projectConfig' => '{parameters: {level: 0, phpVersion: 70300, editorUrl: vscode://file/%%file%%:%%line%%, tmpDir: /var/www/html/packages/products-service/.tools/phpstan, reportUnmatchedIgnoredErrors: false, paths: [/var/www/html/packages/products-service], excludePaths: {analyseAndScan: [/var/www/html/packages/products-service/.git, /var/www/html/packages/products-service/.temp, /var/www/html/packages/products-service/.tools, /var/www/html/packages/products-service/.vscode, /var/www/html/packages/products-service/vendor, /var/www/html/packages/products-service/clash, /var/www/html/packages/products-service/codegolf, /var/www/html/packages/products-service/contest], analyse: []}, dynamicConstantNames: [DEBUG, PERF_LOG]}}',
  'analysedPaths' => 
  array (
    0 => '/var/www/html/packages/products-service',
  ),
  'scannedFiles' => 
  array (
  ),
  'composerLocks' => 
  array (
    '/var/www/html/packages/products-service/composer.lock' => 'd198e65eabcbca7251fe91cfee94e5c678185d89',
  ),
  'composerInstalled' => 
  array (
    '/var/www/html/packages/products-service/vendor/composer/installed.php' => 
    array (
      'versions' => 
      array (
        'brick/math' => 
        array (
          'pretty_version' => '0.10.2',
          'version' => '0.10.2.0',
          'reference' => '459f2781e1a08d52ee56b0b1444086e038561e3f',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../brick/math',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'cordoval/hamcrest-php' => 
        array (
          'dev_requirement' => true,
          'replaced' => 
          array (
            0 => '*',
          ),
        ),
        'davedevelopment/hamcrest-php' => 
        array (
          'dev_requirement' => true,
          'replaced' => 
          array (
            0 => '*',
          ),
        ),
        'dflydev/dot-access-data' => 
        array (
          'pretty_version' => 'v3.0.2',
          'version' => '3.0.2.0',
          'reference' => 'f41715465d65213d644d3141a6a93081be5d3549',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../dflydev/dot-access-data',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'doctrine/inflector' => 
        array (
          'pretty_version' => '2.0.6',
          'version' => '2.0.6.0',
          'reference' => 'd9d313a36c872fd6ee06d9a6cbcf713eaa40f024',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../doctrine/inflector',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'doctrine/instantiator' => 
        array (
          'pretty_version' => '1.4.1',
          'version' => '1.4.1.0',
          'reference' => '10dcfce151b967d20fde1b34ae6640712c3891bc',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../doctrine/instantiator',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'doctrine/lexer' => 
        array (
          'pretty_version' => '1.2.3',
          'version' => '1.2.3.0',
          'reference' => 'c268e882d4dbdd85e36e4ad69e02dc284f89d229',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../doctrine/lexer',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'dragonmantank/cron-expression' => 
        array (
          'pretty_version' => 'v3.3.2',
          'version' => '3.3.2.0',
          'reference' => '782ca5968ab8b954773518e9e49a6f892a34b2a8',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../dragonmantank/cron-expression',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'egulias/email-validator' => 
        array (
          'pretty_version' => '2.1.25',
          'version' => '2.1.25.0',
          'reference' => '0dbf5d78455d4d6a41d186da50adc1122ec066f4',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../egulias/email-validator',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'fakerphp/faker' => 
        array (
          'pretty_version' => 'v1.20.0',
          'version' => '1.20.0.0',
          'reference' => '37f751c67a5372d4e26353bd9384bc03744ec77b',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../fakerphp/faker',
          'aliases' => 
          array (
          ),
          'dev_requirement' => false,
        ),
        'graham-campbell/result-type' => 
        array (
          'pretty_version' => 'v1.1.0',
          'version' => '1.1.0.0',
          'reference' => 'a878d45c1914464426dc94da61c9e1d36ae262a8',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../graham-campbell/result-type',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'guzzlehttp/psr7' => 
        array (
          'pretty_version' => '2.4.3',
          'version' => '2.4.3.0',
          'reference' => '67c26b443f348a51926030c83481b85718457d3d',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../guzzlehttp/psr7',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'hamcrest/hamcrest-php' => 
        array (
          'pretty_version' => 'v2.0.1',
          'version' => '2.0.1.0',
          'reference' => '8c3d0a3f6af734494ad8f6fbbee0ba92422859f3',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../hamcrest/hamcrest-php',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'illuminate/auth' => 
        array (
          'dev_requirement' => true,
          'replaced' => 
          array (
            0 => 'v8.83.26',
          ),
        ),
        'illuminate/broadcasting' => 
        array (
          'dev_requirement' => true,
          'replaced' => 
          array (
            0 => 'v8.83.26',
          ),
        ),
        'illuminate/bus' => 
        array (
          'dev_requirement' => true,
          'replaced' => 
          array (
            0 => 'v8.83.26',
          ),
        ),
        'illuminate/cache' => 
        array (
          'dev_requirement' => true,
          'replaced' => 
          array (
            0 => 'v8.83.26',
          ),
        ),
        'illuminate/collections' => 
        array (
          'dev_requirement' => true,
          'replaced' => 
          array (
            0 => 'v8.83.26',
          ),
        ),
        'illuminate/config' => 
        array (
          'dev_requirement' => true,
          'replaced' => 
          array (
            0 => 'v8.83.26',
          ),
        ),
        'illuminate/console' => 
        array (
          'dev_requirement' => true,
          'replaced' => 
          array (
            0 => 'v8.83.26',
          ),
        ),
        'illuminate/container' => 
        array (
          'dev_requirement' => true,
          'replaced' => 
          array (
            0 => 'v8.83.26',
          ),
        ),
        'illuminate/contracts' => 
        array (
          'dev_requirement' => true,
          'replaced' => 
          array (
            0 => 'v8.83.26',
          ),
        ),
        'illuminate/cookie' => 
        array (
          'dev_requirement' => true,
          'replaced' => 
          array (
            0 => 'v8.83.26',
          ),
        ),
        'illuminate/database' => 
        array (
          'dev_requirement' => true,
          'replaced' => 
          array (
            0 => 'v8.83.26',
          ),
        ),
        'illuminate/encryption' => 
        array (
          'dev_requirement' => true,
          'replaced' => 
          array (
            0 => 'v8.83.26',
          ),
        ),
        'illuminate/events' => 
        array (
          'dev_requirement' => true,
          'replaced' => 
          array (
            0 => 'v8.83.26',
          ),
        ),
        'illuminate/filesystem' => 
        array (
          'dev_requirement' => true,
          'replaced' => 
          array (
            0 => 'v8.83.26',
          ),
        ),
        'illuminate/hashing' => 
        array (
          'dev_requirement' => true,
          'replaced' => 
          array (
            0 => 'v8.83.26',
          ),
        ),
        'illuminate/http' => 
        array (
          'dev_requirement' => true,
          'replaced' => 
          array (
            0 => 'v8.83.26',
          ),
        ),
        'illuminate/log' => 
        array (
          'dev_requirement' => true,
          'replaced' => 
          array (
            0 => 'v8.83.26',
          ),
        ),
        'illuminate/macroable' => 
        array (
          'dev_requirement' => true,
          'replaced' => 
          array (
            0 => 'v8.83.26',
          ),
        ),
        'illuminate/mail' => 
        array (
          'dev_requirement' => true,
          'replaced' => 
          array (
            0 => 'v8.83.26',
          ),
        ),
        'illuminate/notifications' => 
        array (
          'dev_requirement' => true,
          'replaced' => 
          array (
            0 => 'v8.83.26',
          ),
        ),
        'illuminate/pagination' => 
        array (
          'dev_requirement' => true,
          'replaced' => 
          array (
            0 => 'v8.83.26',
          ),
        ),
        'illuminate/pipeline' => 
        array (
          'dev_requirement' => true,
          'replaced' => 
          array (
            0 => 'v8.83.26',
          ),
        ),
        'illuminate/queue' => 
        array (
          'dev_requirement' => true,
          'replaced' => 
          array (
            0 => 'v8.83.26',
          ),
        ),
        'illuminate/redis' => 
        array (
          'dev_requirement' => true,
          'replaced' => 
          array (
            0 => 'v8.83.26',
          ),
        ),
        'illuminate/routing' => 
        array (
          'dev_requirement' => true,
          'replaced' => 
          array (
            0 => 'v8.83.26',
          ),
        ),
        'illuminate/session' => 
        array (
          'dev_requirement' => true,
          'replaced' => 
          array (
            0 => 'v8.83.26',
          ),
        ),
        'illuminate/support' => 
        array (
          'dev_requirement' => true,
          'replaced' => 
          array (
            0 => 'v8.83.26',
          ),
        ),
        'illuminate/testing' => 
        array (
          'dev_requirement' => true,
          'replaced' => 
          array (
            0 => 'v8.83.26',
          ),
        ),
        'illuminate/translation' => 
        array (
          'dev_requirement' => true,
          'replaced' => 
          array (
            0 => 'v8.83.26',
          ),
        ),
        'illuminate/validation' => 
        array (
          'dev_requirement' => true,
          'replaced' => 
          array (
            0 => 'v8.83.26',
          ),
        ),
        'illuminate/view' => 
        array (
          'dev_requirement' => true,
          'replaced' => 
          array (
            0 => 'v8.83.26',
          ),
        ),
        'kodova/hamcrest-php' => 
        array (
          'dev_requirement' => true,
          'replaced' => 
          array (
            0 => '*',
          ),
        ),
        'laravel/framework' => 
        array (
          'pretty_version' => 'v8.83.26',
          'version' => '8.83.26.0',
          'reference' => '7411d9fa71c1b0fd73a33e225f14512b74e6c81e',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../laravel/framework',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'laravel/serializable-closure' => 
        array (
          'pretty_version' => 'v1.2.2',
          'version' => '1.2.2.0',
          'reference' => '47afb7fae28ed29057fdca37e16a84f90cc62fae',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../laravel/serializable-closure',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'league/commonmark' => 
        array (
          'pretty_version' => '2.3.7',
          'version' => '2.3.7.0',
          'reference' => 'a36bd2be4f5387c0f3a8792a0d76b7d68865abbf',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../league/commonmark',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'league/config' => 
        array (
          'pretty_version' => 'v1.1.1',
          'version' => '1.1.1.0',
          'reference' => 'a9d39eeeb6cc49d10a6e6c36f22c4c1f4a767f3e',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../league/config',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'league/flysystem' => 
        array (
          'pretty_version' => '1.1.10',
          'version' => '1.1.10.0',
          'reference' => '3239285c825c152bcc315fe0e87d6b55f5972ed1',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../league/flysystem',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'league/mime-type-detection' => 
        array (
          'pretty_version' => '1.11.0',
          'version' => '1.11.0.0',
          'reference' => 'ff6248ea87a9f116e78edd6002e39e5128a0d4dd',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../league/mime-type-detection',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'mockery/mockery' => 
        array (
          'pretty_version' => '1.5.1',
          'version' => '1.5.1.0',
          'reference' => 'e92dcc83d5a51851baf5f5591d32cb2b16e3684e',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../mockery/mockery',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'monolog/monolog' => 
        array (
          'pretty_version' => '2.8.0',
          'version' => '2.8.0.0',
          'reference' => '720488632c590286b88b80e62aa3d3d551ad4a50',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../monolog/monolog',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'mtdowling/cron-expression' => 
        array (
          'dev_requirement' => true,
          'replaced' => 
          array (
            0 => '^1.0',
          ),
        ),
        'myclabs/deep-copy' => 
        array (
          'pretty_version' => '1.11.0',
          'version' => '1.11.0.0',
          'reference' => '14daed4296fae74d9e3201d2c4925d1acb7aa614',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../myclabs/deep-copy',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'nesbot/carbon' => 
        array (
          'pretty_version' => '2.64.0',
          'version' => '2.64.0.0',
          'reference' => '889546413c97de2d05063b8cb7b193c2531ea211',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../nesbot/carbon',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'nette/schema' => 
        array (
          'pretty_version' => 'v1.2.3',
          'version' => '1.2.3.0',
          'reference' => 'abbdbb70e0245d5f3bf77874cea1dfb0c930d06f',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../nette/schema',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'nette/utils' => 
        array (
          'pretty_version' => 'v3.2.8',
          'version' => '3.2.8.0',
          'reference' => '02a54c4c872b99e4ec05c4aec54b5a06eb0f6368',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../nette/utils',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'nikic/php-parser' => 
        array (
          'pretty_version' => 'v4.15.2',
          'version' => '4.15.2.0',
          'reference' => 'f59bbe44bf7d96f24f3e2b4ddc21cd52c1d2adbc',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../nikic/php-parser',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'opis/closure' => 
        array (
          'pretty_version' => '3.6.3',
          'version' => '3.6.3.0',
          'reference' => '3d81e4309d2a927abbe66df935f4bb60082805ad',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../opis/closure',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'orchestra/testbench' => 
        array (
          'pretty_version' => 'v6.25.1',
          'version' => '6.25.1.0',
          'reference' => '0516123d26d64117bc04f7e9cb982eae2624e750',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../orchestra/testbench',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'orchestra/testbench-core' => 
        array (
          'pretty_version' => 'v6.29.1',
          'version' => '6.29.1.0',
          'reference' => '29a7586915885f89b8d2203efe20f76afe9cf956',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../orchestra/testbench-core',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'phar-io/manifest' => 
        array (
          'pretty_version' => '2.0.3',
          'version' => '2.0.3.0',
          'reference' => '97803eca37d319dfa7826cc2437fc020857acb53',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../phar-io/manifest',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'phar-io/version' => 
        array (
          'pretty_version' => '3.2.1',
          'version' => '3.2.1.0',
          'reference' => '4f7fd7836c6f332bb2933569e566a0d6c4cbed74',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../phar-io/version',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'phpoption/phpoption' => 
        array (
          'pretty_version' => '1.9.0',
          'version' => '1.9.0.0',
          'reference' => 'dc5ff11e274a90cc1c743f66c9ad700ce50db9ab',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../phpoption/phpoption',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'phpstan/phpstan' => 
        array (
          'pretty_version' => '1.9.2',
          'version' => '1.9.2.0',
          'reference' => 'd6fdf01c53978b6429f1393ba4afeca39cc68afa',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../phpstan/phpstan',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'phpunit/php-code-coverage' => 
        array (
          'pretty_version' => '9.2.19',
          'version' => '9.2.19.0',
          'reference' => 'c77b56b63e3d2031bd8997fcec43c1925ae46559',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../phpunit/php-code-coverage',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'phpunit/php-file-iterator' => 
        array (
          'pretty_version' => '3.0.6',
          'version' => '3.0.6.0',
          'reference' => 'cf1c2e7c203ac650e352f4cc675a7021e7d1b3cf',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../phpunit/php-file-iterator',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'phpunit/php-invoker' => 
        array (
          'pretty_version' => '3.1.1',
          'version' => '3.1.1.0',
          'reference' => '5a10147d0aaf65b58940a0b72f71c9ac0423cc67',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../phpunit/php-invoker',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'phpunit/php-text-template' => 
        array (
          'pretty_version' => '2.0.4',
          'version' => '2.0.4.0',
          'reference' => '5da5f67fc95621df9ff4c4e5a84d6a8a2acf7c28',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../phpunit/php-text-template',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'phpunit/php-timer' => 
        array (
          'pretty_version' => '5.0.3',
          'version' => '5.0.3.0',
          'reference' => '5a63ce20ed1b5bf577850e2c4e87f4aa902afbd2',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../phpunit/php-timer',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'phpunit/phpunit' => 
        array (
          'pretty_version' => '9.5.26',
          'version' => '9.5.26.0',
          'reference' => '851867efcbb6a1b992ec515c71cdcf20d895e9d2',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../phpunit/phpunit',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'pimple/pimple' => 
        array (
          'pretty_version' => 'v3.5.0',
          'version' => '3.5.0.0',
          'reference' => 'a94b3a4db7fb774b3d78dad2315ddc07629e1bed',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../pimple/pimple',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'psr/container' => 
        array (
          'pretty_version' => '1.1.2',
          'version' => '1.1.2.0',
          'reference' => '513e0666f7216c7459170d56df27dfcefe1689ea',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../psr/container',
          'aliases' => 
          array (
          ),
          'dev_requirement' => false,
        ),
        'psr/container-implementation' => 
        array (
          'dev_requirement' => true,
          'provided' => 
          array (
            0 => '1.0',
          ),
        ),
        'psr/event-dispatcher' => 
        array (
          'pretty_version' => '1.0.0',
          'version' => '1.0.0.0',
          'reference' => 'dbefd12671e8a14ec7f180cab83036ed26714bb0',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../psr/event-dispatcher',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'psr/event-dispatcher-implementation' => 
        array (
          'dev_requirement' => true,
          'provided' => 
          array (
            0 => '1.0',
          ),
        ),
        'psr/http-factory' => 
        array (
          'pretty_version' => '1.0.1',
          'version' => '1.0.1.0',
          'reference' => '12ac7fcd07e5b077433f5f2bee95b3a771bf61be',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../psr/http-factory',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'psr/http-factory-implementation' => 
        array (
          'dev_requirement' => true,
          'provided' => 
          array (
            0 => '1.0',
          ),
        ),
        'psr/http-message' => 
        array (
          'pretty_version' => '1.0.1',
          'version' => '1.0.1.0',
          'reference' => 'f6561bf28d520154e4b0ec72be95418abe6d9363',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../psr/http-message',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'psr/http-message-implementation' => 
        array (
          'dev_requirement' => true,
          'provided' => 
          array (
            0 => '1.0',
          ),
        ),
        'psr/log' => 
        array (
          'pretty_version' => '2.0.0',
          'version' => '2.0.0.0',
          'reference' => 'ef29f6d262798707a9edd554e2b82517ef3a9376',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../psr/log',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'psr/log-implementation' => 
        array (
          'dev_requirement' => true,
          'provided' => 
          array (
            0 => '1.0.0 || 2.0.0 || 3.0.0',
            1 => '1.0|2.0',
          ),
        ),
        'psr/simple-cache' => 
        array (
          'pretty_version' => '1.0.1',
          'version' => '1.0.1.0',
          'reference' => '408d5eafb83c57f6365a3ca330ff23aa4a5fa39b',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../psr/simple-cache',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'psr/simple-cache-implementation' => 
        array (
          'dev_requirement' => true,
          'provided' => 
          array (
            0 => '1.0',
          ),
        ),
        'ralouphie/getallheaders' => 
        array (
          'pretty_version' => '3.0.3',
          'version' => '3.0.3.0',
          'reference' => '120b605dfeb996808c31b6477290a714d356e822',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../ralouphie/getallheaders',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'ramsey/collection' => 
        array (
          'pretty_version' => '1.2.2',
          'version' => '1.2.2.0',
          'reference' => 'cccc74ee5e328031b15640b51056ee8d3bb66c0a',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../ramsey/collection',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'ramsey/uuid' => 
        array (
          'pretty_version' => '4.6.0',
          'version' => '4.6.0.0',
          'reference' => 'ad63bc700e7d021039e30ce464eba384c4a1d40f',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../ramsey/uuid',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'rhumsaa/uuid' => 
        array (
          'dev_requirement' => true,
          'replaced' => 
          array (
            0 => '4.6.0',
          ),
        ),
        'sebastian/cli-parser' => 
        array (
          'pretty_version' => '1.0.1',
          'version' => '1.0.1.0',
          'reference' => '442e7c7e687e42adc03470c7b668bc4b2402c0b2',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../sebastian/cli-parser',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'sebastian/code-unit' => 
        array (
          'pretty_version' => '1.0.8',
          'version' => '1.0.8.0',
          'reference' => '1fc9f64c0927627ef78ba436c9b17d967e68e120',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../sebastian/code-unit',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'sebastian/code-unit-reverse-lookup' => 
        array (
          'pretty_version' => '2.0.3',
          'version' => '2.0.3.0',
          'reference' => 'ac91f01ccec49fb77bdc6fd1e548bc70f7faa3e5',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../sebastian/code-unit-reverse-lookup',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'sebastian/comparator' => 
        array (
          'pretty_version' => '4.0.8',
          'version' => '4.0.8.0',
          'reference' => 'fa0f136dd2334583309d32b62544682ee972b51a',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../sebastian/comparator',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'sebastian/complexity' => 
        array (
          'pretty_version' => '2.0.2',
          'version' => '2.0.2.0',
          'reference' => '739b35e53379900cc9ac327b2147867b8b6efd88',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../sebastian/complexity',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'sebastian/diff' => 
        array (
          'pretty_version' => '4.0.4',
          'version' => '4.0.4.0',
          'reference' => '3461e3fccc7cfdfc2720be910d3bd73c69be590d',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../sebastian/diff',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'sebastian/environment' => 
        array (
          'pretty_version' => '5.1.4',
          'version' => '5.1.4.0',
          'reference' => '1b5dff7bb151a4db11d49d90e5408e4e938270f7',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../sebastian/environment',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'sebastian/exporter' => 
        array (
          'pretty_version' => '4.0.5',
          'version' => '4.0.5.0',
          'reference' => 'ac230ed27f0f98f597c8a2b6eb7ac563af5e5b9d',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../sebastian/exporter',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'sebastian/global-state' => 
        array (
          'pretty_version' => '5.0.5',
          'version' => '5.0.5.0',
          'reference' => '0ca8db5a5fc9c8646244e629625ac486fa286bf2',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../sebastian/global-state',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'sebastian/lines-of-code' => 
        array (
          'pretty_version' => '1.0.3',
          'version' => '1.0.3.0',
          'reference' => 'c1c2e997aa3146983ed888ad08b15470a2e22ecc',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../sebastian/lines-of-code',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'sebastian/object-enumerator' => 
        array (
          'pretty_version' => '4.0.4',
          'version' => '4.0.4.0',
          'reference' => '5c9eeac41b290a3712d88851518825ad78f45c71',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../sebastian/object-enumerator',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'sebastian/object-reflector' => 
        array (
          'pretty_version' => '2.0.4',
          'version' => '2.0.4.0',
          'reference' => 'b4f479ebdbf63ac605d183ece17d8d7fe49c15c7',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../sebastian/object-reflector',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'sebastian/recursion-context' => 
        array (
          'pretty_version' => '4.0.4',
          'version' => '4.0.4.0',
          'reference' => 'cd9d8cf3c5804de4341c283ed787f099f5506172',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../sebastian/recursion-context',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'sebastian/resource-operations' => 
        array (
          'pretty_version' => '3.0.3',
          'version' => '3.0.3.0',
          'reference' => '0f4443cb3a1d92ce809899753bc0d5d5a8dd19a8',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../sebastian/resource-operations',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'sebastian/type' => 
        array (
          'pretty_version' => '3.2.0',
          'version' => '3.2.0.0',
          'reference' => 'fb3fe09c5f0bae6bc27ef3ce933a1e0ed9464b6e',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../sebastian/type',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'sebastian/version' => 
        array (
          'pretty_version' => '3.0.2',
          'version' => '3.0.2.0',
          'reference' => 'c6c1022351a901512170118436c764e473f6de8c',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../sebastian/version',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'spatie/backtrace' => 
        array (
          'pretty_version' => '1.2.1',
          'version' => '1.2.1.0',
          'reference' => '4ee7d41aa5268107906ea8a4d9ceccde136dbd5b',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../spatie/backtrace',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'spatie/laravel-ray' => 
        array (
          'pretty_version' => '1.31.0',
          'version' => '1.31.0.0',
          'reference' => '7394694afd89d05879e7a69c54abab73c1199acd',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../spatie/laravel-ray',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'spatie/macroable' => 
        array (
          'pretty_version' => '2.0.0',
          'version' => '2.0.0.0',
          'reference' => 'ec2c320f932e730607aff8052c44183cf3ecb072',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../spatie/macroable',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'spatie/ray' => 
        array (
          'pretty_version' => '1.36.0',
          'version' => '1.36.0.0',
          'reference' => '4a4def8cda4806218341b8204c98375aa8c34323',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../spatie/ray',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'squizlabs/php_codesniffer' => 
        array (
          'pretty_version' => '3.7.1',
          'version' => '3.7.1.0',
          'reference' => '1359e176e9307e906dc3d890bcc9603ff6d90619',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../squizlabs/php_codesniffer',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'swiftmailer/swiftmailer' => 
        array (
          'pretty_version' => 'v6.3.0',
          'version' => '6.3.0.0',
          'reference' => '8a5d5072dca8f48460fce2f4131fcc495eec654c',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../swiftmailer/swiftmailer',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'symfony/console' => 
        array (
          'pretty_version' => 'v5.4.16',
          'version' => '5.4.16.0',
          'reference' => '8e9b9c8dfb33af6057c94e1b44846bee700dc5ef',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../symfony/console',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'symfony/css-selector' => 
        array (
          'pretty_version' => 'v6.0.11',
          'version' => '6.0.11.0',
          'reference' => 'ab2746acddc4f03a7234c8441822ac5d5c63efe9',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../symfony/css-selector',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'symfony/deprecation-contracts' => 
        array (
          'pretty_version' => 'v3.0.2',
          'version' => '3.0.2.0',
          'reference' => '26954b3d62a6c5fd0ea8a2a00c0353a14978d05c',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../symfony/deprecation-contracts',
          'aliases' => 
          array (
          ),
          'dev_requirement' => false,
        ),
        'symfony/error-handler' => 
        array (
          'pretty_version' => 'v5.4.15',
          'version' => '5.4.15.0',
          'reference' => '539cf1428b8442303c6e876ad7bf5a7babd91091',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../symfony/error-handler',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'symfony/event-dispatcher' => 
        array (
          'pretty_version' => 'v6.0.9',
          'version' => '6.0.9.0',
          'reference' => '5c85b58422865d42c6eb46f7693339056db098a8',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../symfony/event-dispatcher',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'symfony/event-dispatcher-contracts' => 
        array (
          'pretty_version' => 'v3.0.2',
          'version' => '3.0.2.0',
          'reference' => '7bc61cc2db649b4637d331240c5346dcc7708051',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../symfony/event-dispatcher-contracts',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'symfony/event-dispatcher-implementation' => 
        array (
          'dev_requirement' => true,
          'provided' => 
          array (
            0 => '2.0|3.0',
          ),
        ),
        'symfony/finder' => 
        array (
          'pretty_version' => 'v5.4.11',
          'version' => '5.4.11.0',
          'reference' => '7872a66f57caffa2916a584db1aa7f12adc76f8c',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../symfony/finder',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'symfony/http-foundation' => 
        array (
          'pretty_version' => 'v5.4.16',
          'version' => '5.4.16.0',
          'reference' => '5032c5849aef24741e1970cb03511b0dd131d838',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../symfony/http-foundation',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'symfony/http-kernel' => 
        array (
          'pretty_version' => 'v5.4.16',
          'version' => '5.4.16.0',
          'reference' => 'b432c57c5de73634b1859093c1f58e3cd84455a1',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../symfony/http-kernel',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'symfony/mime' => 
        array (
          'pretty_version' => 'v5.4.16',
          'version' => '5.4.16.0',
          'reference' => '46eeedb08f0832b1b61a84c612d945fc85ee4734',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../symfony/mime',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'symfony/polyfill-ctype' => 
        array (
          'pretty_version' => 'v1.27.0',
          'version' => '1.27.0.0',
          'reference' => '5bbc823adecdae860bb64756d639ecfec17b050a',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../symfony/polyfill-ctype',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'symfony/polyfill-iconv' => 
        array (
          'pretty_version' => 'v1.27.0',
          'version' => '1.27.0.0',
          'reference' => '927013f3aac555983a5059aada98e1907d842695',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../symfony/polyfill-iconv',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'symfony/polyfill-intl-grapheme' => 
        array (
          'pretty_version' => 'v1.27.0',
          'version' => '1.27.0.0',
          'reference' => '511a08c03c1960e08a883f4cffcacd219b758354',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../symfony/polyfill-intl-grapheme',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'symfony/polyfill-intl-idn' => 
        array (
          'pretty_version' => 'v1.27.0',
          'version' => '1.27.0.0',
          'reference' => '639084e360537a19f9ee352433b84ce831f3d2da',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../symfony/polyfill-intl-idn',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'symfony/polyfill-intl-normalizer' => 
        array (
          'pretty_version' => 'v1.27.0',
          'version' => '1.27.0.0',
          'reference' => '19bd1e4fcd5b91116f14d8533c57831ed00571b6',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../symfony/polyfill-intl-normalizer',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'symfony/polyfill-mbstring' => 
        array (
          'pretty_version' => 'v1.27.0',
          'version' => '1.27.0.0',
          'reference' => '8ad114f6b39e2c98a8b0e3bd907732c207c2b534',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../symfony/polyfill-mbstring',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'symfony/polyfill-php72' => 
        array (
          'pretty_version' => 'v1.27.0',
          'version' => '1.27.0.0',
          'reference' => '869329b1e9894268a8a61dabb69153029b7a8c97',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../symfony/polyfill-php72',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'symfony/polyfill-php73' => 
        array (
          'pretty_version' => 'v1.27.0',
          'version' => '1.27.0.0',
          'reference' => '9e8ecb5f92152187c4799efd3c96b78ccab18ff9',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../symfony/polyfill-php73',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'symfony/polyfill-php80' => 
        array (
          'pretty_version' => 'v1.27.0',
          'version' => '1.27.0.0',
          'reference' => '7a6ff3f1959bb01aefccb463a0f2cd3d3d2fd936',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../symfony/polyfill-php80',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'symfony/polyfill-php81' => 
        array (
          'pretty_version' => 'v1.27.0',
          'version' => '1.27.0.0',
          'reference' => '707403074c8ea6e2edaf8794b0157a0bfa52157a',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../symfony/polyfill-php81',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'symfony/process' => 
        array (
          'pretty_version' => 'v5.4.11',
          'version' => '5.4.11.0',
          'reference' => '6e75fe6874cbc7e4773d049616ab450eff537bf1',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../symfony/process',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'symfony/routing' => 
        array (
          'pretty_version' => 'v5.4.15',
          'version' => '5.4.15.0',
          'reference' => '5c9b129efe9abce9470e384bf65d8a7e262eee69',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../symfony/routing',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'symfony/service-contracts' => 
        array (
          'pretty_version' => 'v2.5.2',
          'version' => '2.5.2.0',
          'reference' => '4b426aac47d6427cc1a1d0f7e2ac724627f5966c',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../symfony/service-contracts',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'symfony/stopwatch' => 
        array (
          'pretty_version' => 'v6.0.13',
          'version' => '6.0.13.0',
          'reference' => '7554fde6848af5ef1178f8ccbdbdb8ae1092c70a',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../symfony/stopwatch',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'symfony/string' => 
        array (
          'pretty_version' => 'v6.0.15',
          'version' => '6.0.15.0',
          'reference' => '51ac0fa0ccf132a00519b87c97e8f775fa14e771',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../symfony/string',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'symfony/translation' => 
        array (
          'pretty_version' => 'v6.0.14',
          'version' => '6.0.14.0',
          'reference' => '6f99eb179aee4652c0a7cd7c11f2a870d904330c',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../symfony/translation',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'symfony/translation-contracts' => 
        array (
          'pretty_version' => 'v3.0.2',
          'version' => '3.0.2.0',
          'reference' => 'acbfbb274e730e5a0236f619b6168d9dedb3e282',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../symfony/translation-contracts',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'symfony/translation-implementation' => 
        array (
          'dev_requirement' => true,
          'provided' => 
          array (
            0 => '2.3|3.0',
          ),
        ),
        'symfony/var-dumper' => 
        array (
          'pretty_version' => 'v5.4.14',
          'version' => '5.4.14.0',
          'reference' => '6894d06145fefebd9a4c7272baa026a1c394a430',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../symfony/var-dumper',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'symfony/yaml' => 
        array (
          'pretty_version' => 'v5.4.16',
          'version' => '5.4.16.0',
          'reference' => 'ebd37c71f62d5ec5f6e27de3e06fee492d4c6298',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../symfony/yaml',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'theseer/tokenizer' => 
        array (
          'pretty_version' => '1.2.1',
          'version' => '1.2.1.0',
          'reference' => '34a41e998c2183e22995f158c581e7b5e755ab9e',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../theseer/tokenizer',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'tijsverkoyen/css-to-inline-styles' => 
        array (
          'pretty_version' => '2.2.5',
          'version' => '2.2.5.0',
          'reference' => '4348a3a06651827a27d989ad1d13efec6bb49b19',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../tijsverkoyen/css-to-inline-styles',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'vlucas/phpdotenv' => 
        array (
          'pretty_version' => 'v5.5.0',
          'version' => '5.5.0.0',
          'reference' => '1a7ea2afc49c3ee6d87061f5a233e3a035d0eae7',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../vlucas/phpdotenv',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'voku/portable-ascii' => 
        array (
          'pretty_version' => '1.6.1',
          'version' => '1.6.1.0',
          'reference' => '87337c91b9dfacee02452244ee14ab3c43bc485a',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../voku/portable-ascii',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'webmozart/assert' => 
        array (
          'pretty_version' => '1.11.0',
          'version' => '1.11.0.0',
          'reference' => '11cb2199493b2f8a3b53e7f19068fc6aac760991',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../webmozart/assert',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'zbateson/mail-mime-parser' => 
        array (
          'pretty_version' => '2.2.3',
          'version' => '2.2.3.0',
          'reference' => '295c7f82a8c44af685680d9df6714beb812e90ff',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../zbateson/mail-mime-parser',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'zbateson/mb-wrapper' => 
        array (
          'pretty_version' => '1.1.2',
          'version' => '1.1.2.0',
          'reference' => '5d9d190ef18ce6d424e3ac6f5ebe13901f92b74a',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../zbateson/mb-wrapper',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
        'zbateson/stream-decorators' => 
        array (
          'pretty_version' => '1.0.7',
          'version' => '1.0.7.0',
          'reference' => '8f8ca208572963258b7e6d91106181706deacd10',
          'type' => 'library',
          'install_path' => '/var/www/html/packages/products-service/vendor/composer/../zbateson/stream-decorators',
          'aliases' => 
          array (
          ),
          'dev_requirement' => true,
        ),
      ),
    ),
  ),
  'executedFilesHashes' => 
  array (
    'phar:///var/www/html/packages/products-service/vendor/phpstan/phpstan/phpstan.phar/stubs/runtime/Attribute.php' => 'eaf9127f074e9c7ebc65043ec4050f9fed60c2bb',
    'phar:///var/www/html/packages/products-service/vendor/phpstan/phpstan/phpstan.phar/stubs/runtime/ReflectionAttribute.php' => '0b4b78277eb6545955d2ce5e09bff28f1f8052c8',
    'phar:///var/www/html/packages/products-service/vendor/phpstan/phpstan/phpstan.phar/stubs/runtime/ReflectionIntersectionType.php' => 'a3e6299b87ee5d407dae7651758edfa11a74cb11',
    'phar:///var/www/html/packages/products-service/vendor/phpstan/phpstan/phpstan.phar/stubs/runtime/ReflectionUnionType.php' => '1b349aa997a834faeafe05fa21bc31cae22bf2e2',
  ),
  'phpExtensions' => 
  array (
    0 => 'Core',
    1 => 'PDO',
    2 => 'Phar',
    3 => 'Reflection',
    4 => 'SPL',
    5 => 'SimpleXML',
    6 => 'ctype',
    7 => 'curl',
    8 => 'date',
    9 => 'dom',
    10 => 'exif',
    11 => 'fileinfo',
    12 => 'filter',
    13 => 'ftp',
    14 => 'gd',
    15 => 'hash',
    16 => 'iconv',
    17 => 'json',
    18 => 'libxml',
    19 => 'mbstring',
    20 => 'mysqlnd',
    21 => 'openssl',
    22 => 'pcntl',
    23 => 'pcre',
    24 => 'pdo_mysql',
    25 => 'pdo_sqlite',
    26 => 'posix',
    27 => 'readline',
    28 => 'redis',
    29 => 'session',
    30 => 'sodium',
    31 => 'sqlite3',
    32 => 'standard',
    33 => 'tokenizer',
    34 => 'xml',
    35 => 'xmlreader',
    36 => 'xmlwriter',
    37 => 'zip',
    38 => 'zlib',
  ),
  'stubFiles' => 
  array (
  ),
  'level' => '0',
),
	'projectExtensionFiles' => array (
),
	'errorsCallback' => static function (): array { return array (
); },
	'collectedDataCallback' => static function (): array { return array (
); },
	'dependencies' => array (
  '/var/www/html/packages/products-service/config/config.php' => 
  array (
    'fileHash' => '286a4e8cdf365c64e0545da427cdd2ac91006867',
    'dependentFiles' => 
    array (
    ),
  ),
  '/var/www/html/packages/products-service/src/Application/Service/ProductContractTypesList/ProductContractTypesListCommand.php' => 
  array (
    'fileHash' => '8d3c771e7204a4d02c49068037a4635e6398d1cb',
    'dependentFiles' => 
    array (
      0 => '/var/www/html/packages/products-service/src/Providers/Http/Controllers/ProductContractTypesListController.php',
    ),
  ),
  '/var/www/html/packages/products-service/src/Application/Service/ProductContractTypesList/ProductContractTypesListHandler.php' => 
  array (
    'fileHash' => '9c231a2508860c4273b8d0f23bf5391eeeb9e9a8',
    'dependentFiles' => 
    array (
      0 => '/var/www/html/packages/products-service/src/Providers/Http/Controllers/ProductContractTypesListController.php',
    ),
  ),
  '/var/www/html/packages/products-service/src/Application/Service/ProductFiltersList/ProductFiltersListCommand.php' => 
  array (
    'fileHash' => '9ff1a8968657029298643105ac5c19b4fc3f3d82',
    'dependentFiles' => 
    array (
      0 => '/var/www/html/packages/products-service/src/Providers/Http/Controllers/ProductFilterOrderListController.php',
    ),
  ),
  '/var/www/html/packages/products-service/src/Application/Service/ProductFiltersList/ProductFiltersListHandler.php' => 
  array (
    'fileHash' => '351987cb5c3329e0497018ebcaa35958eac34e70',
    'dependentFiles' => 
    array (
      0 => '/var/www/html/packages/products-service/src/Providers/Http/Controllers/ProductFilterOrderListController.php',
    ),
  ),
  '/var/www/html/packages/products-service/src/Application/Service/ProductPrinceRangeList/ProductPrinceRangeListCommand.php' => 
  array (
    'fileHash' => 'b5e2679d46a72e580a0bee9664bb8910fca56d1e',
    'dependentFiles' => 
    array (
      0 => '/var/www/html/packages/products-service/src/Providers/Http/Controllers/ProductPrinceRangeListController.php',
    ),
  ),
  '/var/www/html/packages/products-service/src/Application/Service/ProductPrinceRangeList/ProductPrinceRangeListHandler.php' => 
  array (
    'fileHash' => '67d2b2c299b66bf17473e83b94076d758ed8b9ae',
    'dependentFiles' => 
    array (
      0 => '/var/www/html/packages/products-service/src/Providers/Http/Controllers/ProductPrinceRangeListController.php',
    ),
  ),
  '/var/www/html/packages/products-service/src/Application/Service/ProductSearch/ProductSearchCommand.php' => 
  array (
    'fileHash' => '0c25112f2cd928e8e41ff98e416cb651a9098a01',
    'dependentFiles' => 
    array (
      0 => '/var/www/html/packages/products-service/src/Providers/Http/Controllers/ProductSearchController.php',
    ),
  ),
  '/var/www/html/packages/products-service/src/Application/Service/ProductSearch/ProductSearchHandler.php' => 
  array (
    'fileHash' => 'c8347f215c5b1080cbbaecfb7fe99f0a6f763599',
    'dependentFiles' => 
    array (
      0 => '/var/www/html/packages/products-service/src/Providers/Http/Controllers/ProductSearchController.php',
    ),
  ),
  '/var/www/html/packages/products-service/src/Domain/Category.php' => 
  array (
    'fileHash' => 'ad9862ade5c5047fc37627a9b0bc40b648488427',
    'dependentFiles' => 
    array (
      0 => '/var/www/html/packages/products-service/src/Domain/Product.php',
      1 => '/var/www/html/packages/products-service/src/Infraestructure/Databases/Factories/CategoryFactory.php',
      2 => '/var/www/html/packages/products-service/src/Infraestructure/Databases/Seeds/ProductsServiceSeeder.php',
    ),
  ),
  '/var/www/html/packages/products-service/src/Domain/DTO/ProductContractTypeResource.php' => 
  array (
    'fileHash' => '780481db0e55e9f838f3cd99c85061278bbdac0f',
    'dependentFiles' => 
    array (
      0 => '/var/www/html/packages/products-service/src/Providers/Http/Controllers/ProductContractTypesListController.php',
    ),
  ),
  '/var/www/html/packages/products-service/src/Domain/DTO/ProductPriceRangeResource.php' => 
  array (
    'fileHash' => 'd61c9b4db5d2f9e8cbac8926942db3c2b0b22caf',
    'dependentFiles' => 
    array (
      0 => '/var/www/html/packages/products-service/src/Providers/Http/Controllers/ProductPrinceRangeListController.php',
    ),
  ),
  '/var/www/html/packages/products-service/src/Domain/DTO/ProductSearchResource.php' => 
  array (
    'fileHash' => '1c3ad1fde602d2b1aa5a2c6e296fcb22aebb6d53',
    'dependentFiles' => 
    array (
      0 => '/var/www/html/packages/products-service/src/Providers/Http/Controllers/ProductSearchController.php',
    ),
  ),
  '/var/www/html/packages/products-service/src/Domain/Events/ProductSearchWasExecuted.php' => 
  array (
    'fileHash' => '7ea75f79bd0ed0db90a4ee93db0b8bb33164f6dc',
    'dependentFiles' => 
    array (
      0 => '/var/www/html/packages/products-service/src/Providers/Http/Controllers/ProductSearchController.php',
    ),
  ),
  '/var/www/html/packages/products-service/src/Domain/Filters/AllowHideaway.php' => 
  array (
    'fileHash' => 'b96f1047dc5f80a6d282ee65aad9e03a033c6633',
    'dependentFiles' => 
    array (
    ),
  ),
  '/var/www/html/packages/products-service/src/Domain/Filters/AllowLoad.php' => 
  array (
    'fileHash' => 'c08345babaad0cde3abb1ba33d7d218fcff88f25',
    'dependentFiles' => 
    array (
    ),
  ),
  '/var/www/html/packages/products-service/src/Domain/Filters/AllowNoLicense.php' => 
  array (
    'fileHash' => 'd962476fc56c169e79d34a399dd140ba7d89e3ac',
    'dependentFiles' => 
    array (
    ),
  ),
  '/var/www/html/packages/products-service/src/Domain/Filters/Category.php' => 
  array (
    'fileHash' => '2307ba6ccf0f70ec25fb94ed61d083017e4af11c',
    'dependentFiles' => 
    array (
    ),
  ),
  '/var/www/html/packages/products-service/src/Domain/Filters/ContractType.php' => 
  array (
    'fileHash' => '930f41f675d7bfad7562a4b414ac286daba569b2',
    'dependentFiles' => 
    array (
    ),
  ),
  '/var/www/html/packages/products-service/src/Domain/Filters/PriceFrom.php' => 
  array (
    'fileHash' => '1e1d4c8a73e9e8f8c2d8b479f9751f97854e6edd',
    'dependentFiles' => 
    array (
    ),
  ),
  '/var/www/html/packages/products-service/src/Domain/Filters/PriceTo.php' => 
  array (
    'fileHash' => '165682b98d801435b2f7c405dd1522384aeca1cd',
    'dependentFiles' => 
    array (
    ),
  ),
  '/var/www/html/packages/products-service/src/Domain/Product.php' => 
  array (
    'fileHash' => 'e1bc835585a01feb7ddc53a82733d6099dbc322f',
    'dependentFiles' => 
    array (
      0 => '/var/www/html/packages/products-service/src/Application/Service/ProductSearch/ProductSearchHandler.php',
      1 => '/var/www/html/packages/products-service/src/Domain/Category.php',
      2 => '/var/www/html/packages/products-service/src/Domain/ProductContract.php',
      3 => '/var/www/html/packages/products-service/src/Domain/ProductRating.php',
      4 => '/var/www/html/packages/products-service/src/Infraestructure/Databases/Factories/ProductFactory.php',
      5 => '/var/www/html/packages/products-service/src/Infraestructure/Databases/Seeds/ProductsServiceSeeder.php',
    ),
  ),
  '/var/www/html/packages/products-service/src/Domain/ProductContract.php' => 
  array (
    'fileHash' => '088b66431e0018c025212ff969ea267feeffbdf2',
    'dependentFiles' => 
    array (
      0 => '/var/www/html/packages/products-service/src/Application/Service/ProductContractTypesList/ProductContractTypesListHandler.php',
      1 => '/var/www/html/packages/products-service/src/Application/Service/ProductPrinceRangeList/ProductPrinceRangeListHandler.php',
      2 => '/var/www/html/packages/products-service/src/Domain/Product.php',
      3 => '/var/www/html/packages/products-service/src/Infraestructure/Databases/Factories/ProductContractFactory.php',
      4 => '/var/www/html/packages/products-service/src/Infraestructure/Databases/Seeds/ProductsServiceSeeder.php',
    ),
  ),
  '/var/www/html/packages/products-service/src/Domain/ProductRating.php' => 
  array (
    'fileHash' => '7565d63eda850d17ab88184bdb7dee13b686fbaa',
    'dependentFiles' => 
    array (
      0 => '/var/www/html/packages/products-service/src/Domain/Product.php',
      1 => '/var/www/html/packages/products-service/src/Infraestructure/Databases/Factories/ProductRatingFactory.php',
      2 => '/var/www/html/packages/products-service/src/Infraestructure/Databases/Seeds/ProductsServiceSeeder.php',
    ),
  ),
  '/var/www/html/packages/products-service/src/Domain/ProductSearch.php' => 
  array (
    'fileHash' => 'cc84252654cf9aec2c3b66cdbd252eaea4e31825',
    'dependentFiles' => 
    array (
      0 => '/var/www/html/packages/products-service/src/Application/Service/ProductSearch/ProductSearchHandler.php',
      1 => '/var/www/html/packages/products-service/src/Domain/Category.php',
      2 => '/var/www/html/packages/products-service/src/Domain/Product.php',
      3 => '/var/www/html/packages/products-service/src/Domain/ProductContract.php',
      4 => '/var/www/html/packages/products-service/src/Domain/ProductRating.php',
      5 => '/var/www/html/packages/products-service/src/Infraestructure/Databases/Factories/ProductFactory.php',
      6 => '/var/www/html/packages/products-service/src/Infraestructure/Databases/Seeds/ProductsServiceSeeder.php',
    ),
  ),
  '/var/www/html/packages/products-service/src/Infraestructure/Databases/Factories/CategoryFactory.php' => 
  array (
    'fileHash' => '563f57d2d8b326672bd9e9879cb26c11627848cd',
    'dependentFiles' => 
    array (
      0 => '/var/www/html/packages/products-service/src/Domain/Category.php',
    ),
  ),
  '/var/www/html/packages/products-service/src/Infraestructure/Databases/Factories/ProductContractFactory.php' => 
  array (
    'fileHash' => 'f9f69f67262d22014a71d6d7391d9623b5b687de',
    'dependentFiles' => 
    array (
      0 => '/var/www/html/packages/products-service/src/Domain/ProductContract.php',
    ),
  ),
  '/var/www/html/packages/products-service/src/Infraestructure/Databases/Factories/ProductFactory.php' => 
  array (
    'fileHash' => '35d9f1f3ed001be50070d955bd47357f125f8fa1',
    'dependentFiles' => 
    array (
      0 => '/var/www/html/packages/products-service/src/Domain/Product.php',
    ),
  ),
  '/var/www/html/packages/products-service/src/Infraestructure/Databases/Factories/ProductRatingFactory.php' => 
  array (
    'fileHash' => 'f62f19bbdb55b9c2a773928d847161fc4bf606f6',
    'dependentFiles' => 
    array (
      0 => '/var/www/html/packages/products-service/src/Domain/ProductRating.php',
    ),
  ),
  '/var/www/html/packages/products-service/src/Infraestructure/Databases/Migrations/2022_12_04_220659_create_categories_table.php' => 
  array (
    'fileHash' => '8b71ba11f7428ce497a4431674ac5e6da8d5f716',
    'dependentFiles' => 
    array (
    ),
  ),
  '/var/www/html/packages/products-service/src/Infraestructure/Databases/Migrations/2022_12_04_220670_create_products_table.php' => 
  array (
    'fileHash' => '0adc6795e50868b4ab554687f239340aa73fac76',
    'dependentFiles' => 
    array (
    ),
  ),
  '/var/www/html/packages/products-service/src/Infraestructure/Databases/Migrations/2022_12_05_183917_create_product_ratings_table.php' => 
  array (
    'fileHash' => '6704ce6d5dead7fe917f8983da9fbd30078b38b9',
    'dependentFiles' => 
    array (
    ),
  ),
  '/var/www/html/packages/products-service/src/Infraestructure/Databases/Migrations/2022_12_05_184128_create_product_contracts_table.php' => 
  array (
    'fileHash' => 'f9a992d9db9f957b8211e5ccf883e92a6ef23ded',
    'dependentFiles' => 
    array (
    ),
  ),
  '/var/www/html/packages/products-service/src/Infraestructure/Databases/Seeds/ProductsServiceSeeder.php' => 
  array (
    'fileHash' => '74887eedf962fd8930aa9f609a0c9215a9435ca8',
    'dependentFiles' => 
    array (
    ),
  ),
  '/var/www/html/packages/products-service/src/Providers/Http/Controllers/Controller.php' => 
  array (
    'fileHash' => '8851e4ff45c6d554d8f24c69b515fd04d232f921',
    'dependentFiles' => 
    array (
      0 => '/var/www/html/packages/products-service/src/Providers/Http/Controllers/ProductContractTypesListController.php',
      1 => '/var/www/html/packages/products-service/src/Providers/Http/Controllers/ProductFilterOrderListController.php',
      2 => '/var/www/html/packages/products-service/src/Providers/Http/Controllers/ProductPrinceRangeListController.php',
      3 => '/var/www/html/packages/products-service/src/Providers/Http/Controllers/ProductSearchController.php',
      4 => '/var/www/html/packages/products-service/src/Providers/Routes/web.php',
    ),
  ),
  '/var/www/html/packages/products-service/src/Providers/Http/Controllers/ProductContractTypesListController.php' => 
  array (
    'fileHash' => 'f0e28d5bd7bc5cedf56d946333370aca09f05dc5',
    'dependentFiles' => 
    array (
      0 => '/var/www/html/packages/products-service/src/Providers/Routes/web.php',
    ),
  ),
  '/var/www/html/packages/products-service/src/Providers/Http/Controllers/ProductFilterOrderListController.php' => 
  array (
    'fileHash' => '7c45a14db67189837876a23831ae605d18c83835',
    'dependentFiles' => 
    array (
      0 => '/var/www/html/packages/products-service/src/Providers/Routes/web.php',
    ),
  ),
  '/var/www/html/packages/products-service/src/Providers/Http/Controllers/ProductPrinceRangeListController.php' => 
  array (
    'fileHash' => '87e31980358a7f28c2b01c3f8894e3cf303107b3',
    'dependentFiles' => 
    array (
      0 => '/var/www/html/packages/products-service/src/Providers/Routes/web.php',
    ),
  ),
  '/var/www/html/packages/products-service/src/Providers/Http/Controllers/ProductSearchController.php' => 
  array (
    'fileHash' => '43840cc5634192e87f0d8496533a26cd7719be09',
    'dependentFiles' => 
    array (
      0 => '/var/www/html/packages/products-service/src/Providers/Routes/web.php',
    ),
  ),
  '/var/www/html/packages/products-service/src/Providers/Middlewares/AfterCacheSearchMiddleware.php' => 
  array (
    'fileHash' => '9f357a5eb03dda9bee5f6718ced1b14e17892855',
    'dependentFiles' => 
    array (
      0 => '/var/www/html/packages/products-service/src/Providers/ProductServiceProvider.php',
    ),
  ),
  '/var/www/html/packages/products-service/src/Providers/Middlewares/BeforeCacheSearchMiddleware.php' => 
  array (
    'fileHash' => '2e0f8f0dadfd257a4ca991a79c0528d553a2f630',
    'dependentFiles' => 
    array (
      0 => '/var/www/html/packages/products-service/src/Providers/ProductServiceProvider.php',
    ),
  ),
  '/var/www/html/packages/products-service/src/Providers/ProductServiceProvider.php' => 
  array (
    'fileHash' => '8043af6ecba33a1669ab91e1897a089b6200f35a',
    'dependentFiles' => 
    array (
      0 => '/var/www/html/packages/products-service/tests/TestCase.php',
    ),
  ),
  '/var/www/html/packages/products-service/src/Providers/Routes/web.php' => 
  array (
    'fileHash' => '3592fac3c56105fbf054c8ef6ecb4f2fd5e8b96d',
    'dependentFiles' => 
    array (
    ),
  ),
  '/var/www/html/packages/products-service/src/Share/IDDDCommand.php' => 
  array (
    'fileHash' => '18886331373ddfb054911a11ee223dd3014bd87c',
    'dependentFiles' => 
    array (
      0 => '/var/www/html/packages/products-service/src/Application/Service/ProductContractTypesList/ProductContractTypesListCommand.php',
      1 => '/var/www/html/packages/products-service/src/Application/Service/ProductFiltersList/ProductFiltersListCommand.php',
      2 => '/var/www/html/packages/products-service/src/Application/Service/ProductPrinceRangeList/ProductPrinceRangeListCommand.php',
      3 => '/var/www/html/packages/products-service/src/Application/Service/ProductSearch/ProductSearchCommand.php',
      4 => '/var/www/html/packages/products-service/src/Providers/Http/Controllers/ProductContractTypesListController.php',
      5 => '/var/www/html/packages/products-service/src/Providers/Http/Controllers/ProductFilterOrderListController.php',
      6 => '/var/www/html/packages/products-service/src/Providers/Http/Controllers/ProductPrinceRangeListController.php',
      7 => '/var/www/html/packages/products-service/src/Providers/Http/Controllers/ProductSearchController.php',
    ),
  ),
  '/var/www/html/packages/products-service/src/Share/IDDDCommandHandlers.php' => 
  array (
    'fileHash' => '2410dd3c347ba4e066159f02891323f704c53e8d',
    'dependentFiles' => 
    array (
      0 => '/var/www/html/packages/products-service/src/Application/Service/ProductContractTypesList/ProductContractTypesListHandler.php',
      1 => '/var/www/html/packages/products-service/src/Application/Service/ProductFiltersList/ProductFiltersListHandler.php',
      2 => '/var/www/html/packages/products-service/src/Application/Service/ProductPrinceRangeList/ProductPrinceRangeListHandler.php',
      3 => '/var/www/html/packages/products-service/src/Application/Service/ProductSearch/ProductSearchHandler.php',
      4 => '/var/www/html/packages/products-service/src/Providers/Http/Controllers/ProductContractTypesListController.php',
      5 => '/var/www/html/packages/products-service/src/Providers/Http/Controllers/ProductFilterOrderListController.php',
      6 => '/var/www/html/packages/products-service/src/Providers/Http/Controllers/ProductPrinceRangeListController.php',
      7 => '/var/www/html/packages/products-service/src/Providers/Http/Controllers/ProductSearchController.php',
    ),
  ),
  '/var/www/html/packages/products-service/tests/Feature/ProductSearchTest.php' => 
  array (
    'fileHash' => '9ef784e9d8d9a3ebb07216081b7ad17e6c1f6645',
    'dependentFiles' => 
    array (
    ),
  ),
  '/var/www/html/packages/products-service/tests/TestCase.php' => 
  array (
    'fileHash' => 'fa367f02e383a94cef274b9ad1ebeba3a294efbc',
    'dependentFiles' => 
    array (
      0 => '/var/www/html/packages/products-service/tests/Feature/ProductSearchTest.php',
    ),
  ),
),
	'exportedNodesCallback' => static function (): array { return array (
  '/var/www/html/packages/products-service/src/Application/Service/ProductContractTypesList/ProductContractTypesListCommand.php' => 
  array (
    0 => 
    PHPStan\Dependency\ExportedNode\ExportedClassNode::__set_state(array(
       'name' => 'VinciarelliFranco\\ProductService\\Application\\Service\\ProductContractTypesList\\ProductContractTypesListCommand',
       'phpDoc' => NULL,
       'abstract' => false,
       'final' => false,
       'extends' => NULL,
       'implements' => 
      array (
        0 => 'VinciarelliFranco\\ProductService\\Share\\IDDDCommand',
      ),
       'usedTraits' => 
      array (
      ),
       'traitUseAdaptations' => 
      array (
      ),
       'statements' => 
      array (
        0 => 
        PHPStan\Dependency\ExportedNode\ExportedMethodNode::__set_state(array(
           'name' => 'fromWebRequest',
           'phpDoc' => NULL,
           'byRef' => false,
           'public' => true,
           'private' => false,
           'abstract' => false,
           'final' => false,
           'static' => true,
           'returnType' => 'VinciarelliFranco\\ProductService\\Application\\Service\\ProductContractTypesList\\ProductContractTypesListCommand',
           'parameters' => 
          array (
            0 => 
            PHPStan\Dependency\ExportedNode\ExportedParameterNode::__set_state(array(
               'name' => 'data',
               'type' => '?array',
               'byRef' => false,
               'variadic' => false,
               'hasDefault' => false,
               'attributes' => 
              array (
              ),
            )),
          ),
           'attributes' => 
          array (
          ),
        )),
        1 => 
        PHPStan\Dependency\ExportedNode\ExportedMethodNode::__set_state(array(
           'name' => 'getAll',
           'phpDoc' => NULL,
           'byRef' => false,
           'public' => true,
           'private' => false,
           'abstract' => false,
           'final' => false,
           'static' => false,
           'returnType' => 'array',
           'parameters' => 
          array (
          ),
           'attributes' => 
          array (
          ),
        )),
      ),
       'attributes' => 
      array (
      ),
    )),
  ),
  '/var/www/html/packages/products-service/src/Application/Service/ProductContractTypesList/ProductContractTypesListHandler.php' => 
  array (
    0 => 
    PHPStan\Dependency\ExportedNode\ExportedClassNode::__set_state(array(
       'name' => 'VinciarelliFranco\\ProductService\\Application\\Service\\ProductContractTypesList\\ProductContractTypesListHandler',
       'phpDoc' => NULL,
       'abstract' => false,
       'final' => false,
       'extends' => NULL,
       'implements' => 
      array (
        0 => 'VinciarelliFranco\\ProductService\\Share\\IDDDCommandHandlers',
      ),
       'usedTraits' => 
      array (
      ),
       'traitUseAdaptations' => 
      array (
      ),
       'statements' => 
      array (
        0 => 
        PHPStan\Dependency\ExportedNode\ExportedMethodNode::__set_state(array(
           'name' => 'handle',
           'phpDoc' => NULL,
           'byRef' => false,
           'public' => true,
           'private' => false,
           'abstract' => false,
           'final' => false,
           'static' => true,
           'returnType' => NULL,
           'parameters' => 
          array (
            0 => 
            PHPStan\Dependency\ExportedNode\ExportedParameterNode::__set_state(array(
               'name' => 'command',
               'type' => NULL,
               'byRef' => false,
               'variadic' => false,
               'hasDefault' => false,
               'attributes' => 
              array (
              ),
            )),
          ),
           'attributes' => 
          array (
          ),
        )),
      ),
       'attributes' => 
      array (
      ),
    )),
  ),
  '/var/www/html/packages/products-service/src/Application/Service/ProductFiltersList/ProductFiltersListCommand.php' => 
  array (
    0 => 
    PHPStan\Dependency\ExportedNode\ExportedClassNode::__set_state(array(
       'name' => 'VinciarelliFranco\\ProductService\\Application\\Service\\ProductFiltersList\\ProductFiltersListCommand',
       'phpDoc' => NULL,
       'abstract' => false,
       'final' => false,
       'extends' => NULL,
       'implements' => 
      array (
        0 => 'VinciarelliFranco\\ProductService\\Share\\IDDDCommand',
      ),
       'usedTraits' => 
      array (
      ),
       'traitUseAdaptations' => 
      array (
      ),
       'statements' => 
      array (
        0 => 
        PHPStan\Dependency\ExportedNode\ExportedMethodNode::__set_state(array(
           'name' => 'fromWebRequest',
           'phpDoc' => NULL,
           'byRef' => false,
           'public' => true,
           'private' => false,
           'abstract' => false,
           'final' => false,
           'static' => true,
           'returnType' => 'VinciarelliFranco\\ProductService\\Application\\Service\\ProductFiltersList\\ProductFiltersListCommand',
           'parameters' => 
          array (
            0 => 
            PHPStan\Dependency\ExportedNode\ExportedParameterNode::__set_state(array(
               'name' => 'data',
               'type' => '?array',
               'byRef' => false,
               'variadic' => false,
               'hasDefault' => false,
               'attributes' => 
              array (
              ),
            )),
          ),
           'attributes' => 
          array (
          ),
        )),
        1 => 
        PHPStan\Dependency\ExportedNode\ExportedMethodNode::__set_state(array(
           'name' => 'getAll',
           'phpDoc' => NULL,
           'byRef' => false,
           'public' => true,
           'private' => false,
           'abstract' => false,
           'final' => false,
           'static' => false,
           'returnType' => 'array',
           'parameters' => 
          array (
          ),
           'attributes' => 
          array (
          ),
        )),
      ),
       'attributes' => 
      array (
      ),
    )),
  ),
  '/var/www/html/packages/products-service/src/Application/Service/ProductFiltersList/ProductFiltersListHandler.php' => 
  array (
    0 => 
    PHPStan\Dependency\ExportedNode\ExportedClassNode::__set_state(array(
       'name' => 'VinciarelliFranco\\ProductService\\Application\\Service\\ProductFiltersList\\ProductFiltersListHandler',
       'phpDoc' => NULL,
       'abstract' => false,
       'final' => false,
       'extends' => NULL,
       'implements' => 
      array (
        0 => 'VinciarelliFranco\\ProductService\\Share\\IDDDCommandHandlers',
      ),
       'usedTraits' => 
      array (
      ),
       'traitUseAdaptations' => 
      array (
      ),
       'statements' => 
      array (
        0 => 
        PHPStan\Dependency\ExportedNode\ExportedMethodNode::__set_state(array(
           'name' => 'handle',
           'phpDoc' => NULL,
           'byRef' => false,
           'public' => true,
           'private' => false,
           'abstract' => false,
           'final' => false,
           'static' => true,
           'returnType' => NULL,
           'parameters' => 
          array (
            0 => 
            PHPStan\Dependency\ExportedNode\ExportedParameterNode::__set_state(array(
               'name' => 'command',
               'type' => NULL,
               'byRef' => false,
               'variadic' => false,
               'hasDefault' => false,
               'attributes' => 
              array (
              ),
            )),
          ),
           'attributes' => 
          array (
          ),
        )),
      ),
       'attributes' => 
      array (
      ),
    )),
  ),
  '/var/www/html/packages/products-service/src/Application/Service/ProductPrinceRangeList/ProductPrinceRangeListCommand.php' => 
  array (
    0 => 
    PHPStan\Dependency\ExportedNode\ExportedClassNode::__set_state(array(
       'name' => 'VinciarelliFranco\\ProductService\\Application\\Service\\ProductPrinceRangeList\\ProductPrinceRangeListCommand',
       'phpDoc' => NULL,
       'abstract' => false,
       'final' => false,
       'extends' => NULL,
       'implements' => 
      array (
        0 => 'VinciarelliFranco\\ProductService\\Share\\IDDDCommand',
      ),
       'usedTraits' => 
      array (
      ),
       'traitUseAdaptations' => 
      array (
      ),
       'statements' => 
      array (
        0 => 
        PHPStan\Dependency\ExportedNode\ExportedMethodNode::__set_state(array(
           'name' => 'fromWebRequest',
           'phpDoc' => NULL,
           'byRef' => false,
           'public' => true,
           'private' => false,
           'abstract' => false,
           'final' => false,
           'static' => true,
           'returnType' => 'VinciarelliFranco\\ProductService\\Application\\Service\\ProductPrinceRangeList\\ProductPrinceRangeListCommand',
           'parameters' => 
          array (
            0 => 
            PHPStan\Dependency\ExportedNode\ExportedParameterNode::__set_state(array(
               'name' => 'data',
               'type' => '?array',
               'byRef' => false,
               'variadic' => false,
               'hasDefault' => false,
               'attributes' => 
              array (
              ),
            )),
          ),
           'attributes' => 
          array (
          ),
        )),
        1 => 
        PHPStan\Dependency\ExportedNode\ExportedMethodNode::__set_state(array(
           'name' => 'getAll',
           'phpDoc' => NULL,
           'byRef' => false,
           'public' => true,
           'private' => false,
           'abstract' => false,
           'final' => false,
           'static' => false,
           'returnType' => 'array',
           'parameters' => 
          array (
          ),
           'attributes' => 
          array (
          ),
        )),
      ),
       'attributes' => 
      array (
      ),
    )),
  ),
  '/var/www/html/packages/products-service/src/Application/Service/ProductPrinceRangeList/ProductPrinceRangeListHandler.php' => 
  array (
    0 => 
    PHPStan\Dependency\ExportedNode\ExportedClassNode::__set_state(array(
       'name' => 'VinciarelliFranco\\ProductService\\Application\\Service\\ProductPrinceRangeList\\ProductPrinceRangeListHandler',
       'phpDoc' => NULL,
       'abstract' => false,
       'final' => false,
       'extends' => NULL,
       'implements' => 
      array (
        0 => 'VinciarelliFranco\\ProductService\\Share\\IDDDCommandHandlers',
      ),
       'usedTraits' => 
      array (
      ),
       'traitUseAdaptations' => 
      array (
      ),
       'statements' => 
      array (
        0 => 
        PHPStan\Dependency\ExportedNode\ExportedMethodNode::__set_state(array(
           'name' => 'handle',
           'phpDoc' => NULL,
           'byRef' => false,
           'public' => true,
           'private' => false,
           'abstract' => false,
           'final' => false,
           'static' => true,
           'returnType' => NULL,
           'parameters' => 
          array (
            0 => 
            PHPStan\Dependency\ExportedNode\ExportedParameterNode::__set_state(array(
               'name' => 'command',
               'type' => NULL,
               'byRef' => false,
               'variadic' => false,
               'hasDefault' => false,
               'attributes' => 
              array (
              ),
            )),
          ),
           'attributes' => 
          array (
          ),
        )),
      ),
       'attributes' => 
      array (
      ),
    )),
  ),
  '/var/www/html/packages/products-service/src/Application/Service/ProductSearch/ProductSearchCommand.php' => 
  array (
    0 => 
    PHPStan\Dependency\ExportedNode\ExportedClassNode::__set_state(array(
       'name' => 'VinciarelliFranco\\ProductService\\Application\\Service\\ProductSearch\\ProductSearchCommand',
       'phpDoc' => NULL,
       'abstract' => false,
       'final' => false,
       'extends' => NULL,
       'implements' => 
      array (
        0 => 'VinciarelliFranco\\ProductService\\Share\\IDDDCommand',
      ),
       'usedTraits' => 
      array (
      ),
       'traitUseAdaptations' => 
      array (
      ),
       'statements' => 
      array (
        0 => 
        PHPStan\Dependency\ExportedNode\ExportedPropertiesNode::__set_state(array(
           'names' => 
          array (
            0 => 'from_record',
          ),
           'phpDoc' => NULL,
           'type' => 'int',
           'public' => false,
           'private' => false,
           'static' => false,
           'readonly' => false,
           'attributes' => 
          array (
          ),
        )),
        1 => 
        PHPStan\Dependency\ExportedNode\ExportedPropertiesNode::__set_state(array(
           'names' => 
          array (
            0 => 'to_record',
          ),
           'phpDoc' => NULL,
           'type' => 'int',
           'public' => false,
           'private' => false,
           'static' => false,
           'readonly' => false,
           'attributes' => 
          array (
          ),
        )),
        2 => 
        PHPStan\Dependency\ExportedNode\ExportedPropertiesNode::__set_state(array(
           'names' => 
          array (
            0 => 'price_from',
          ),
           'phpDoc' => NULL,
           'type' => '?float',
           'public' => false,
           'private' => false,
           'static' => false,
           'readonly' => false,
           'attributes' => 
          array (
          ),
        )),
        3 => 
        PHPStan\Dependency\ExportedNode\ExportedPropertiesNode::__set_state(array(
           'names' => 
          array (
            0 => 'price_to',
          ),
           'phpDoc' => NULL,
           'type' => '?float',
           'public' => false,
           'private' => false,
           'static' => false,
           'readonly' => false,
           'attributes' => 
          array (
          ),
        )),
        4 => 
        PHPStan\Dependency\ExportedNode\ExportedPropertiesNode::__set_state(array(
           'names' => 
          array (
            0 => 'category_id',
          ),
           'phpDoc' => NULL,
           'type' => '?int',
           'public' => false,
           'private' => false,
           'static' => false,
           'readonly' => false,
           'attributes' => 
          array (
          ),
        )),
        5 => 
        PHPStan\Dependency\ExportedNode\ExportedPropertiesNode::__set_state(array(
           'names' => 
          array (
            0 => 'allow_load',
          ),
           'phpDoc' => NULL,
           'type' => '?int',
           'public' => false,
           'private' => false,
           'static' => false,
           'readonly' => false,
           'attributes' => 
          array (
          ),
        )),
        6 => 
        PHPStan\Dependency\ExportedNode\ExportedPropertiesNode::__set_state(array(
           'names' => 
          array (
            0 => 'allow_hideaway',
          ),
           'phpDoc' => NULL,
           'type' => '?int',
           'public' => false,
           'private' => false,
           'static' => false,
           'readonly' => false,
           'attributes' => 
          array (
          ),
        )),
        7 => 
        PHPStan\Dependency\ExportedNode\ExportedPropertiesNode::__set_state(array(
           'names' => 
          array (
            0 => 'allow_no_license',
          ),
           'phpDoc' => NULL,
           'type' => '?int',
           'public' => false,
           'private' => false,
           'static' => false,
           'readonly' => false,
           'attributes' => 
          array (
          ),
        )),
        8 => 
        PHPStan\Dependency\ExportedNode\ExportedPropertiesNode::__set_state(array(
           'names' => 
          array (
            0 => 'contract_type',
          ),
           'phpDoc' => NULL,
           'type' => '?int',
           'public' => false,
           'private' => false,
           'static' => false,
           'readonly' => false,
           'attributes' => 
          array (
          ),
        )),
        9 => 
        PHPStan\Dependency\ExportedNode\ExportedPropertiesNode::__set_state(array(
           'names' => 
          array (
            0 => 'order_by_field',
          ),
           'phpDoc' => NULL,
           'type' => 'array',
           'public' => false,
           'private' => false,
           'static' => false,
           'readonly' => false,
           'attributes' => 
          array (
          ),
        )),
        10 => 
        PHPStan\Dependency\ExportedNode\ExportedClassConstantsNode::__set_state(array(
           'constants' => 
          array (
            0 => 
            PHPStan\Dependency\ExportedNode\ExportedClassConstantNode::__set_state(array(
               'name' => 'DEFAULT_LIMIT_RECORDSET',
               'value' => '10',
               'attributes' => 
              array (
              ),
            )),
          ),
           'public' => true,
           'private' => false,
           'final' => false,
           'phpDoc' => NULL,
        )),
        11 => 
        PHPStan\Dependency\ExportedNode\ExportedClassConstantsNode::__set_state(array(
           'constants' => 
          array (
            0 => 
            PHPStan\Dependency\ExportedNode\ExportedClassConstantNode::__set_state(array(
               'name' => 'DEFAULT_START_RECORDSET',
               'value' => '1',
               'attributes' => 
              array (
              ),
            )),
          ),
           'public' => true,
           'private' => false,
           'final' => false,
           'phpDoc' => NULL,
        )),
        12 => 
        PHPStan\Dependency\ExportedNode\ExportedClassConstantsNode::__set_state(array(
           'constants' => 
          array (
            0 => 
            PHPStan\Dependency\ExportedNode\ExportedClassConstantNode::__set_state(array(
               'name' => 'DEFAULT_ORDER_RECORDSET',
               'value' => '\'ID_ASC\'',
               'attributes' => 
              array (
              ),
            )),
          ),
           'public' => true,
           'private' => false,
           'final' => false,
           'phpDoc' => NULL,
        )),
        13 => 
        PHPStan\Dependency\ExportedNode\ExportedClassConstantsNode::__set_state(array(
           'constants' => 
          array (
            0 => 
            PHPStan\Dependency\ExportedNode\ExportedClassConstantNode::__set_state(array(
               'name' => 'RULES',
               'value' => '[\'from\' => \'numeric\', \'to\' => \'numeric\', \'price_from\' => \'numeric\', \'price_to\' => \'numeric\', \'category\' => \'numeric\', \'allow_load\' => \'numeric\', \'allow_hideaway\' => \'numeric\', \'allow_no_license\' => \'numeric\', \'contract_type\' => \'numeric\', \'orderby\' => \'string\']',
               'attributes' => 
              array (
              ),
            )),
          ),
           'public' => false,
           'private' => false,
           'final' => false,
           'phpDoc' => NULL,
        )),
        14 => 
        PHPStan\Dependency\ExportedNode\ExportedMethodNode::__set_state(array(
           'name' => 'fromWebRequest',
           'phpDoc' => NULL,
           'byRef' => false,
           'public' => true,
           'private' => false,
           'abstract' => false,
           'final' => false,
           'static' => true,
           'returnType' => 'VinciarelliFranco\\ProductService\\Application\\Service\\ProductSearch\\ProductSearchCommand',
           'parameters' => 
          array (
            0 => 
            PHPStan\Dependency\ExportedNode\ExportedParameterNode::__set_state(array(
               'name' => 'data',
               'type' => '?array',
               'byRef' => false,
               'variadic' => false,
               'hasDefault' => false,
               'attributes' => 
              array (
              ),
            )),
          ),
           'attributes' => 
          array (
          ),
        )),
        15 => 
        PHPStan\Dependency\ExportedNode\ExportedMethodNode::__set_state(array(
           'name' => 'getFrom',
           'phpDoc' => NULL,
           'byRef' => false,
           'public' => true,
           'private' => false,
           'abstract' => false,
           'final' => false,
           'static' => false,
           'returnType' => 'int',
           'parameters' => 
          array (
          ),
           'attributes' => 
          array (
          ),
        )),
        16 => 
        PHPStan\Dependency\ExportedNode\ExportedMethodNode::__set_state(array(
           'name' => 'getTo',
           'phpDoc' => NULL,
           'byRef' => false,
           'public' => true,
           'private' => false,
           'abstract' => false,
           'final' => false,
           'static' => false,
           'returnType' => 'int',
           'parameters' => 
          array (
          ),
           'attributes' => 
          array (
          ),
        )),
        17 => 
        PHPStan\Dependency\ExportedNode\ExportedMethodNode::__set_state(array(
           'name' => 'getFromPrice',
           'phpDoc' => NULL,
           'byRef' => false,
           'public' => true,
           'private' => false,
           'abstract' => false,
           'final' => false,
           'static' => false,
           'returnType' => '?float',
           'parameters' => 
          array (
          ),
           'attributes' => 
          array (
          ),
        )),
        18 => 
        PHPStan\Dependency\ExportedNode\ExportedMethodNode::__set_state(array(
           'name' => 'getToPrice',
           'phpDoc' => NULL,
           'byRef' => false,
           'public' => true,
           'private' => false,
           'abstract' => false,
           'final' => false,
           'static' => false,
           'returnType' => '?float',
           'parameters' => 
          array (
          ),
           'attributes' => 
          array (
          ),
        )),
        19 => 
        PHPStan\Dependency\ExportedNode\ExportedMethodNode::__set_state(array(
           'name' => 'getCategoryId',
           'phpDoc' => NULL,
           'byRef' => false,
           'public' => true,
           'private' => false,
           'abstract' => false,
           'final' => false,
           'static' => false,
           'returnType' => '?int',
           'parameters' => 
          array (
          ),
           'attributes' => 
          array (
          ),
        )),
        20 => 
        PHPStan\Dependency\ExportedNode\ExportedMethodNode::__set_state(array(
           'name' => 'getAll',
           'phpDoc' => NULL,
           'byRef' => false,
           'public' => true,
           'private' => false,
           'abstract' => false,
           'final' => false,
           'static' => false,
           'returnType' => 'array',
           'parameters' => 
          array (
          ),
           'attributes' => 
          array (
          ),
        )),
      ),
       'attributes' => 
      array (
      ),
    )),
  ),
  '/var/www/html/packages/products-service/src/Application/Service/ProductSearch/ProductSearchHandler.php' => 
  array (
    0 => 
    PHPStan\Dependency\ExportedNode\ExportedClassNode::__set_state(array(
       'name' => 'VinciarelliFranco\\ProductService\\Application\\Service\\ProductSearch\\ProductSearchHandler',
       'phpDoc' => NULL,
       'abstract' => false,
       'final' => false,
       'extends' => NULL,
       'implements' => 
      array (
        0 => 'VinciarelliFranco\\ProductService\\Share\\IDDDCommandHandlers',
      ),
       'usedTraits' => 
      array (
      ),
       'traitUseAdaptations' => 
      array (
      ),
       'statements' => 
      array (
        0 => 
        PHPStan\Dependency\ExportedNode\ExportedMethodNode::__set_state(array(
           'name' => 'handle',
           'phpDoc' => NULL,
           'byRef' => false,
           'public' => true,
           'private' => false,
           'abstract' => false,
           'final' => false,
           'static' => true,
           'returnType' => NULL,
           'parameters' => 
          array (
            0 => 
            PHPStan\Dependency\ExportedNode\ExportedParameterNode::__set_state(array(
               'name' => 'command',
               'type' => NULL,
               'byRef' => false,
               'variadic' => false,
               'hasDefault' => false,
               'attributes' => 
              array (
              ),
            )),
          ),
           'attributes' => 
          array (
          ),
        )),
      ),
       'attributes' => 
      array (
      ),
    )),
  ),
  '/var/www/html/packages/products-service/src/Domain/Category.php' => 
  array (
    0 => 
    PHPStan\Dependency\ExportedNode\ExportedClassNode::__set_state(array(
       'name' => 'VinciarelliFranco\\ProductService\\Domain\\Category',
       'phpDoc' => NULL,
       'abstract' => false,
       'final' => false,
       'extends' => 'Illuminate\\Database\\Eloquent\\Model',
       'implements' => 
      array (
      ),
       'usedTraits' => 
      array (
        0 => 'Illuminate\\Database\\Eloquent\\Factories\\HasFactory',
      ),
       'traitUseAdaptations' => 
      array (
      ),
       'statements' => 
      array (
        0 => 
        PHPStan\Dependency\ExportedNode\ExportedMethodNode::__set_state(array(
           'name' => 'products',
           'phpDoc' => NULL,
           'byRef' => false,
           'public' => true,
           'private' => false,
           'abstract' => false,
           'final' => false,
           'static' => false,
           'returnType' => NULL,
           'parameters' => 
          array (
          ),
           'attributes' => 
          array (
          ),
        )),
        1 => 
        PHPStan\Dependency\ExportedNode\ExportedMethodNode::__set_state(array(
           'name' => 'newFactory',
           'phpDoc' => NULL,
           'byRef' => false,
           'public' => false,
           'private' => false,
           'abstract' => false,
           'final' => false,
           'static' => true,
           'returnType' => NULL,
           'parameters' => 
          array (
          ),
           'attributes' => 
          array (
          ),
        )),
      ),
       'attributes' => 
      array (
      ),
    )),
  ),
  '/var/www/html/packages/products-service/src/Domain/DTO/ProductContractTypeResource.php' => 
  array (
    0 => 
    PHPStan\Dependency\ExportedNode\ExportedClassNode::__set_state(array(
       'name' => 'VinciarelliFranco\\ProductService\\Domain\\DTO\\ProductContractTypeResource',
       'phpDoc' => NULL,
       'abstract' => false,
       'final' => false,
       'extends' => 'Illuminate\\Http\\Resources\\Json\\JsonResource',
       'implements' => 
      array (
      ),
       'usedTraits' => 
      array (
      ),
       'traitUseAdaptations' => 
      array (
      ),
       'statements' => 
      array (
        0 => 
        PHPStan\Dependency\ExportedNode\ExportedMethodNode::__set_state(array(
           'name' => 'toArray',
           'phpDoc' => 
          PHPStan\Dependency\ExportedNode\ExportedPhpDocNode::__set_state(array(
             'phpDocString' => '/**
     * Transform the resource into an array.
     *
     * @param  \\Illuminate\\Http\\Request  $request
     * @return array|\\Illuminate\\Contracts\\Support\\Arrayable|\\JsonSerializable
     */',
             'namespace' => 'VinciarelliFranco\\ProductService\\Domain\\DTO',
             'uses' => 
            array (
              'jsonresource' => 'Illuminate\\Http\\Resources\\Json\\JsonResource',
            ),
             'constUses' => 
            array (
            ),
          )),
           'byRef' => false,
           'public' => true,
           'private' => false,
           'abstract' => false,
           'final' => false,
           'static' => false,
           'returnType' => NULL,
           'parameters' => 
          array (
            0 => 
            PHPStan\Dependency\ExportedNode\ExportedParameterNode::__set_state(array(
               'name' => 'request',
               'type' => NULL,
               'byRef' => false,
               'variadic' => false,
               'hasDefault' => false,
               'attributes' => 
              array (
              ),
            )),
          ),
           'attributes' => 
          array (
          ),
        )),
      ),
       'attributes' => 
      array (
      ),
    )),
  ),
  '/var/www/html/packages/products-service/src/Domain/DTO/ProductPriceRangeResource.php' => 
  array (
    0 => 
    PHPStan\Dependency\ExportedNode\ExportedClassNode::__set_state(array(
       'name' => 'VinciarelliFranco\\ProductService\\Domain\\DTO\\ProductPriceRangeResource',
       'phpDoc' => NULL,
       'abstract' => false,
       'final' => false,
       'extends' => 'Illuminate\\Http\\Resources\\Json\\JsonResource',
       'implements' => 
      array (
      ),
       'usedTraits' => 
      array (
      ),
       'traitUseAdaptations' => 
      array (
      ),
       'statements' => 
      array (
        0 => 
        PHPStan\Dependency\ExportedNode\ExportedMethodNode::__set_state(array(
           'name' => 'toArray',
           'phpDoc' => 
          PHPStan\Dependency\ExportedNode\ExportedPhpDocNode::__set_state(array(
             'phpDocString' => '/**
     * Transform the resource into an array.
     *
     * @param  \\Illuminate\\Http\\Request  $request
     * @return array|\\Illuminate\\Contracts\\Support\\Arrayable|\\JsonSerializable
     */',
             'namespace' => 'VinciarelliFranco\\ProductService\\Domain\\DTO',
             'uses' => 
            array (
              'jsonresource' => 'Illuminate\\Http\\Resources\\Json\\JsonResource',
            ),
             'constUses' => 
            array (
            ),
          )),
           'byRef' => false,
           'public' => true,
           'private' => false,
           'abstract' => false,
           'final' => false,
           'static' => false,
           'returnType' => NULL,
           'parameters' => 
          array (
            0 => 
            PHPStan\Dependency\ExportedNode\ExportedParameterNode::__set_state(array(
               'name' => 'request',
               'type' => NULL,
               'byRef' => false,
               'variadic' => false,
               'hasDefault' => false,
               'attributes' => 
              array (
              ),
            )),
          ),
           'attributes' => 
          array (
          ),
        )),
      ),
       'attributes' => 
      array (
      ),
    )),
  ),
  '/var/www/html/packages/products-service/src/Domain/DTO/ProductSearchResource.php' => 
  array (
    0 => 
    PHPStan\Dependency\ExportedNode\ExportedClassNode::__set_state(array(
       'name' => 'VinciarelliFranco\\ProductService\\Domain\\DTO\\ProductSearchResource',
       'phpDoc' => NULL,
       'abstract' => false,
       'final' => false,
       'extends' => 'Illuminate\\Http\\Resources\\Json\\JsonResource',
       'implements' => 
      array (
      ),
       'usedTraits' => 
      array (
      ),
       'traitUseAdaptations' => 
      array (
      ),
       'statements' => 
      array (
        0 => 
        PHPStan\Dependency\ExportedNode\ExportedMethodNode::__set_state(array(
           'name' => 'toArray',
           'phpDoc' => 
          PHPStan\Dependency\ExportedNode\ExportedPhpDocNode::__set_state(array(
             'phpDocString' => '/**
     * Transform the resource into an array.
     *
     * @param  \\Illuminate\\Http\\Request  $request
     * @return array|\\Illuminate\\Contracts\\Support\\Arrayable|\\JsonSerializable
     */',
             'namespace' => 'VinciarelliFranco\\ProductService\\Domain\\DTO',
             'uses' => 
            array (
              'jsonresource' => 'Illuminate\\Http\\Resources\\Json\\JsonResource',
            ),
             'constUses' => 
            array (
            ),
          )),
           'byRef' => false,
           'public' => true,
           'private' => false,
           'abstract' => false,
           'final' => false,
           'static' => false,
           'returnType' => NULL,
           'parameters' => 
          array (
            0 => 
            PHPStan\Dependency\ExportedNode\ExportedParameterNode::__set_state(array(
               'name' => 'request',
               'type' => NULL,
               'byRef' => false,
               'variadic' => false,
               'hasDefault' => false,
               'attributes' => 
              array (
              ),
            )),
          ),
           'attributes' => 
          array (
          ),
        )),
      ),
       'attributes' => 
      array (
      ),
    )),
  ),
  '/var/www/html/packages/products-service/src/Domain/Events/ProductSearchWasExecuted.php' => 
  array (
    0 => 
    PHPStan\Dependency\ExportedNode\ExportedClassNode::__set_state(array(
       'name' => 'VinciarelliFranco\\ProductService\\Domain\\Events\\ProductSearchWasExecuted',
       'phpDoc' => NULL,
       'abstract' => false,
       'final' => false,
       'extends' => NULL,
       'implements' => 
      array (
      ),
       'usedTraits' => 
      array (
        0 => 'Illuminate\\Foundation\\Events\\Dispatchable',
        1 => 'Illuminate\\Queue\\SerializesModels',
      ),
       'traitUseAdaptations' => 
      array (
      ),
       'statements' => 
      array (
        0 => 
        PHPStan\Dependency\ExportedNode\ExportedPropertiesNode::__set_state(array(
           'names' => 
          array (
            0 => 'request',
          ),
           'phpDoc' => NULL,
           'type' => NULL,
           'public' => true,
           'private' => false,
           'static' => false,
           'readonly' => false,
           'attributes' => 
          array (
          ),
        )),
        1 => 
        PHPStan\Dependency\ExportedNode\ExportedMethodNode::__set_state(array(
           'name' => '__construct',
           'phpDoc' => NULL,
           'byRef' => false,
           'public' => true,
           'private' => false,
           'abstract' => false,
           'final' => false,
           'static' => false,
           'returnType' => NULL,
           'parameters' => 
          array (
            0 => 
            PHPStan\Dependency\ExportedNode\ExportedParameterNode::__set_state(array(
               'name' => 'request',
               'type' => 'Illuminate\\Support\\Facades\\Request',
               'byRef' => false,
               'variadic' => false,
               'hasDefault' => false,
               'attributes' => 
              array (
              ),
            )),
          ),
           'attributes' => 
          array (
          ),
        )),
      ),
       'attributes' => 
      array (
      ),
    )),
  ),
  '/var/www/html/packages/products-service/src/Domain/Filters/AllowHideaway.php' => 
  array (
    0 => 
    PHPStan\Dependency\ExportedNode\ExportedClassNode::__set_state(array(
       'name' => 'VinciarelliFranco\\ProductService\\Domain\\Filters\\AllowHideaway',
       'phpDoc' => NULL,
       'abstract' => false,
       'final' => false,
       'extends' => NULL,
       'implements' => 
      array (
      ),
       'usedTraits' => 
      array (
      ),
       'traitUseAdaptations' => 
      array (
      ),
       'statements' => 
      array (
        0 => 
        PHPStan\Dependency\ExportedNode\ExportedMethodNode::__set_state(array(
           'name' => 'apply',
           'phpDoc' => NULL,
           'byRef' => false,
           'public' => true,
           'private' => false,
           'abstract' => false,
           'final' => false,
           'static' => true,
           'returnType' => NULL,
           'parameters' => 
          array (
            0 => 
            PHPStan\Dependency\ExportedNode\ExportedParameterNode::__set_state(array(
               'name' => 'query',
               'type' => 'Illuminate\\Database\\Eloquent\\Builder',
               'byRef' => false,
               'variadic' => false,
               'hasDefault' => false,
               'attributes' => 
              array (
              ),
            )),
            1 => 
            PHPStan\Dependency\ExportedNode\ExportedParameterNode::__set_state(array(
               'name' => 'value',
               'type' => NULL,
               'byRef' => false,
               'variadic' => false,
               'hasDefault' => false,
               'attributes' => 
              array (
              ),
            )),
          ),
           'attributes' => 
          array (
          ),
        )),
      ),
       'attributes' => 
      array (
      ),
    )),
  ),
  '/var/www/html/packages/products-service/src/Domain/Filters/AllowLoad.php' => 
  array (
    0 => 
    PHPStan\Dependency\ExportedNode\ExportedClassNode::__set_state(array(
       'name' => 'VinciarelliFranco\\ProductService\\Domain\\Filters\\AllowLoad',
       'phpDoc' => NULL,
       'abstract' => false,
       'final' => false,
       'extends' => NULL,
       'implements' => 
      array (
      ),
       'usedTraits' => 
      array (
      ),
       'traitUseAdaptations' => 
      array (
      ),
       'statements' => 
      array (
        0 => 
        PHPStan\Dependency\ExportedNode\ExportedMethodNode::__set_state(array(
           'name' => 'apply',
           'phpDoc' => NULL,
           'byRef' => false,
           'public' => true,
           'private' => false,
           'abstract' => false,
           'final' => false,
           'static' => true,
           'returnType' => NULL,
           'parameters' => 
          array (
            0 => 
            PHPStan\Dependency\ExportedNode\ExportedParameterNode::__set_state(array(
               'name' => 'query',
               'type' => 'Illuminate\\Database\\Eloquent\\Builder',
               'byRef' => false,
               'variadic' => false,
               'hasDefault' => false,
               'attributes' => 
              array (
              ),
            )),
            1 => 
            PHPStan\Dependency\ExportedNode\ExportedParameterNode::__set_state(array(
               'name' => 'value',
               'type' => NULL,
               'byRef' => false,
               'variadic' => false,
               'hasDefault' => false,
               'attributes' => 
              array (
              ),
            )),
          ),
           'attributes' => 
          array (
          ),
        )),
      ),
       'attributes' => 
      array (
      ),
    )),
  ),
  '/var/www/html/packages/products-service/src/Domain/Filters/AllowNoLicense.php' => 
  array (
    0 => 
    PHPStan\Dependency\ExportedNode\ExportedClassNode::__set_state(array(
       'name' => 'VinciarelliFranco\\ProductService\\Domain\\Filters\\AllowNoLicense',
       'phpDoc' => NULL,
       'abstract' => false,
       'final' => false,
       'extends' => NULL,
       'implements' => 
      array (
      ),
       'usedTraits' => 
      array (
      ),
       'traitUseAdaptations' => 
      array (
      ),
       'statements' => 
      array (
        0 => 
        PHPStan\Dependency\ExportedNode\ExportedMethodNode::__set_state(array(
           'name' => 'apply',
           'phpDoc' => NULL,
           'byRef' => false,
           'public' => true,
           'private' => false,
           'abstract' => false,
           'final' => false,
           'static' => true,
           'returnType' => NULL,
           'parameters' => 
          array (
            0 => 
            PHPStan\Dependency\ExportedNode\ExportedParameterNode::__set_state(array(
               'name' => 'query',
               'type' => 'Illuminate\\Database\\Eloquent\\Builder',
               'byRef' => false,
               'variadic' => false,
               'hasDefault' => false,
               'attributes' => 
              array (
              ),
            )),
            1 => 
            PHPStan\Dependency\ExportedNode\ExportedParameterNode::__set_state(array(
               'name' => 'value',
               'type' => NULL,
               'byRef' => false,
               'variadic' => false,
               'hasDefault' => false,
               'attributes' => 
              array (
              ),
            )),
          ),
           'attributes' => 
          array (
          ),
        )),
      ),
       'attributes' => 
      array (
      ),
    )),
  ),
  '/var/www/html/packages/products-service/src/Domain/Filters/Category.php' => 
  array (
    0 => 
    PHPStan\Dependency\ExportedNode\ExportedClassNode::__set_state(array(
       'name' => 'VinciarelliFranco\\ProductService\\Domain\\Filters\\Category',
       'phpDoc' => NULL,
       'abstract' => false,
       'final' => false,
       'extends' => NULL,
       'implements' => 
      array (
      ),
       'usedTraits' => 
      array (
      ),
       'traitUseAdaptations' => 
      array (
      ),
       'statements' => 
      array (
        0 => 
        PHPStan\Dependency\ExportedNode\ExportedMethodNode::__set_state(array(
           'name' => 'apply',
           'phpDoc' => NULL,
           'byRef' => false,
           'public' => true,
           'private' => false,
           'abstract' => false,
           'final' => false,
           'static' => true,
           'returnType' => NULL,
           'parameters' => 
          array (
            0 => 
            PHPStan\Dependency\ExportedNode\ExportedParameterNode::__set_state(array(
               'name' => 'query',
               'type' => 'Illuminate\\Database\\Eloquent\\Builder',
               'byRef' => false,
               'variadic' => false,
               'hasDefault' => false,
               'attributes' => 
              array (
              ),
            )),
            1 => 
            PHPStan\Dependency\ExportedNode\ExportedParameterNode::__set_state(array(
               'name' => 'value',
               'type' => NULL,
               'byRef' => false,
               'variadic' => false,
               'hasDefault' => false,
               'attributes' => 
              array (
              ),
            )),
          ),
           'attributes' => 
          array (
          ),
        )),
      ),
       'attributes' => 
      array (
      ),
    )),
  ),
  '/var/www/html/packages/products-service/src/Domain/Filters/ContractType.php' => 
  array (
    0 => 
    PHPStan\Dependency\ExportedNode\ExportedClassNode::__set_state(array(
       'name' => 'VinciarelliFranco\\ProductService\\Domain\\Filters\\ContractType',
       'phpDoc' => NULL,
       'abstract' => false,
       'final' => false,
       'extends' => NULL,
       'implements' => 
      array (
      ),
       'usedTraits' => 
      array (
      ),
       'traitUseAdaptations' => 
      array (
      ),
       'statements' => 
      array (
        0 => 
        PHPStan\Dependency\ExportedNode\ExportedMethodNode::__set_state(array(
           'name' => 'apply',
           'phpDoc' => NULL,
           'byRef' => false,
           'public' => true,
           'private' => false,
           'abstract' => false,
           'final' => false,
           'static' => true,
           'returnType' => NULL,
           'parameters' => 
          array (
            0 => 
            PHPStan\Dependency\ExportedNode\ExportedParameterNode::__set_state(array(
               'name' => 'query',
               'type' => 'Illuminate\\Database\\Eloquent\\Builder',
               'byRef' => false,
               'variadic' => false,
               'hasDefault' => false,
               'attributes' => 
              array (
              ),
            )),
            1 => 
            PHPStan\Dependency\ExportedNode\ExportedParameterNode::__set_state(array(
               'name' => 'value',
               'type' => NULL,
               'byRef' => false,
               'variadic' => false,
               'hasDefault' => false,
               'attributes' => 
              array (
              ),
            )),
          ),
           'attributes' => 
          array (
          ),
        )),
      ),
       'attributes' => 
      array (
      ),
    )),
  ),
  '/var/www/html/packages/products-service/src/Domain/Filters/PriceFrom.php' => 
  array (
    0 => 
    PHPStan\Dependency\ExportedNode\ExportedClassNode::__set_state(array(
       'name' => 'VinciarelliFranco\\ProductService\\Domain\\Filters\\PriceFrom',
       'phpDoc' => NULL,
       'abstract' => false,
       'final' => false,
       'extends' => NULL,
       'implements' => 
      array (
      ),
       'usedTraits' => 
      array (
      ),
       'traitUseAdaptations' => 
      array (
      ),
       'statements' => 
      array (
        0 => 
        PHPStan\Dependency\ExportedNode\ExportedMethodNode::__set_state(array(
           'name' => 'apply',
           'phpDoc' => NULL,
           'byRef' => false,
           'public' => true,
           'private' => false,
           'abstract' => false,
           'final' => false,
           'static' => true,
           'returnType' => NULL,
           'parameters' => 
          array (
            0 => 
            PHPStan\Dependency\ExportedNode\ExportedParameterNode::__set_state(array(
               'name' => 'query',
               'type' => 'Illuminate\\Database\\Eloquent\\Builder',
               'byRef' => false,
               'variadic' => false,
               'hasDefault' => false,
               'attributes' => 
              array (
              ),
            )),
            1 => 
            PHPStan\Dependency\ExportedNode\ExportedParameterNode::__set_state(array(
               'name' => 'value',
               'type' => NULL,
               'byRef' => false,
               'variadic' => false,
               'hasDefault' => false,
               'attributes' => 
              array (
              ),
            )),
          ),
           'attributes' => 
          array (
          ),
        )),
      ),
       'attributes' => 
      array (
      ),
    )),
  ),
  '/var/www/html/packages/products-service/src/Domain/Filters/PriceTo.php' => 
  array (
    0 => 
    PHPStan\Dependency\ExportedNode\ExportedClassNode::__set_state(array(
       'name' => 'VinciarelliFranco\\ProductService\\Domain\\Filters\\PriceTo',
       'phpDoc' => NULL,
       'abstract' => false,
       'final' => false,
       'extends' => NULL,
       'implements' => 
      array (
      ),
       'usedTraits' => 
      array (
      ),
       'traitUseAdaptations' => 
      array (
      ),
       'statements' => 
      array (
        0 => 
        PHPStan\Dependency\ExportedNode\ExportedMethodNode::__set_state(array(
           'name' => 'apply',
           'phpDoc' => NULL,
           'byRef' => false,
           'public' => true,
           'private' => false,
           'abstract' => false,
           'final' => false,
           'static' => true,
           'returnType' => NULL,
           'parameters' => 
          array (
            0 => 
            PHPStan\Dependency\ExportedNode\ExportedParameterNode::__set_state(array(
               'name' => 'query',
               'type' => 'Illuminate\\Database\\Eloquent\\Builder',
               'byRef' => false,
               'variadic' => false,
               'hasDefault' => false,
               'attributes' => 
              array (
              ),
            )),
            1 => 
            PHPStan\Dependency\ExportedNode\ExportedParameterNode::__set_state(array(
               'name' => 'value',
               'type' => NULL,
               'byRef' => false,
               'variadic' => false,
               'hasDefault' => false,
               'attributes' => 
              array (
              ),
            )),
          ),
           'attributes' => 
          array (
          ),
        )),
      ),
       'attributes' => 
      array (
      ),
    )),
  ),
  '/var/www/html/packages/products-service/src/Domain/Product.php' => 
  array (
    0 => 
    PHPStan\Dependency\ExportedNode\ExportedClassNode::__set_state(array(
       'name' => 'VinciarelliFranco\\ProductService\\Domain\\Product',
       'phpDoc' => NULL,
       'abstract' => false,
       'final' => false,
       'extends' => 'Illuminate\\Database\\Eloquent\\Model',
       'implements' => 
      array (
      ),
       'usedTraits' => 
      array (
        0 => 'Illuminate\\Database\\Eloquent\\Factories\\HasFactory',
        1 => 'VinciarelliFranco\\ProductService\\Domain\\ProductSearch',
      ),
       'traitUseAdaptations' => 
      array (
      ),
       'statements' => 
      array (
        0 => 
        PHPStan\Dependency\ExportedNode\ExportedMethodNode::__set_state(array(
           'name' => 'category',
           'phpDoc' => NULL,
           'byRef' => false,
           'public' => true,
           'private' => false,
           'abstract' => false,
           'final' => false,
           'static' => false,
           'returnType' => NULL,
           'parameters' => 
          array (
          ),
           'attributes' => 
          array (
          ),
        )),
        1 => 
        PHPStan\Dependency\ExportedNode\ExportedMethodNode::__set_state(array(
           'name' => 'ratings',
           'phpDoc' => NULL,
           'byRef' => false,
           'public' => true,
           'private' => false,
           'abstract' => false,
           'final' => false,
           'static' => false,
           'returnType' => NULL,
           'parameters' => 
          array (
          ),
           'attributes' => 
          array (
          ),
        )),
        2 => 
        PHPStan\Dependency\ExportedNode\ExportedMethodNode::__set_state(array(
           'name' => 'contracts',
           'phpDoc' => NULL,
           'byRef' => false,
           'public' => true,
           'private' => false,
           'abstract' => false,
           'final' => false,
           'static' => false,
           'returnType' => NULL,
           'parameters' => 
          array (
          ),
           'attributes' => 
          array (
          ),
        )),
        3 => 
        PHPStan\Dependency\ExportedNode\ExportedMethodNode::__set_state(array(
           'name' => 'newFactory',
           'phpDoc' => NULL,
           'byRef' => false,
           'public' => false,
           'private' => false,
           'abstract' => false,
           'final' => false,
           'static' => true,
           'returnType' => NULL,
           'parameters' => 
          array (
          ),
           'attributes' => 
          array (
          ),
        )),
      ),
       'attributes' => 
      array (
      ),
    )),
  ),
  '/var/www/html/packages/products-service/src/Domain/ProductContract.php' => 
  array (
    0 => 
    PHPStan\Dependency\ExportedNode\ExportedClassNode::__set_state(array(
       'name' => 'VinciarelliFranco\\ProductService\\Domain\\ProductContract',
       'phpDoc' => NULL,
       'abstract' => false,
       'final' => false,
       'extends' => 'Illuminate\\Database\\Eloquent\\Model',
       'implements' => 
      array (
      ),
       'usedTraits' => 
      array (
        0 => 'Illuminate\\Database\\Eloquent\\Factories\\HasFactory',
      ),
       'traitUseAdaptations' => 
      array (
      ),
       'statements' => 
      array (
        0 => 
        PHPStan\Dependency\ExportedNode\ExportedMethodNode::__set_state(array(
           'name' => 'product',
           'phpDoc' => NULL,
           'byRef' => false,
           'public' => true,
           'private' => false,
           'abstract' => false,
           'final' => false,
           'static' => false,
           'returnType' => NULL,
           'parameters' => 
          array (
          ),
           'attributes' => 
          array (
          ),
        )),
        1 => 
        PHPStan\Dependency\ExportedNode\ExportedMethodNode::__set_state(array(
           'name' => 'newFactory',
           'phpDoc' => NULL,
           'byRef' => false,
           'public' => false,
           'private' => false,
           'abstract' => false,
           'final' => false,
           'static' => true,
           'returnType' => NULL,
           'parameters' => 
          array (
          ),
           'attributes' => 
          array (
          ),
        )),
      ),
       'attributes' => 
      array (
      ),
    )),
  ),
  '/var/www/html/packages/products-service/src/Domain/ProductRating.php' => 
  array (
    0 => 
    PHPStan\Dependency\ExportedNode\ExportedClassNode::__set_state(array(
       'name' => 'VinciarelliFranco\\ProductService\\Domain\\ProductRating',
       'phpDoc' => NULL,
       'abstract' => false,
       'final' => false,
       'extends' => 'Illuminate\\Database\\Eloquent\\Model',
       'implements' => 
      array (
      ),
       'usedTraits' => 
      array (
        0 => 'Illuminate\\Database\\Eloquent\\Factories\\HasFactory',
      ),
       'traitUseAdaptations' => 
      array (
      ),
       'statements' => 
      array (
        0 => 
        PHPStan\Dependency\ExportedNode\ExportedMethodNode::__set_state(array(
           'name' => 'product',
           'phpDoc' => NULL,
           'byRef' => false,
           'public' => true,
           'private' => false,
           'abstract' => false,
           'final' => false,
           'static' => false,
           'returnType' => NULL,
           'parameters' => 
          array (
          ),
           'attributes' => 
          array (
          ),
        )),
        1 => 
        PHPStan\Dependency\ExportedNode\ExportedMethodNode::__set_state(array(
           'name' => 'newFactory',
           'phpDoc' => NULL,
           'byRef' => false,
           'public' => false,
           'private' => false,
           'abstract' => false,
           'final' => false,
           'static' => true,
           'returnType' => NULL,
           'parameters' => 
          array (
          ),
           'attributes' => 
          array (
          ),
        )),
      ),
       'attributes' => 
      array (
      ),
    )),
  ),
  '/var/www/html/packages/products-service/src/Domain/ProductSearch.php' => 
  array (
    0 => 
    PHPStan\Dependency\ExportedNode\ExportedTraitNode::__set_state(array(
       'traitName' => 'VinciarelliFranco\\ProductService\\Domain\\ProductSearch',
    )),
  ),
  '/var/www/html/packages/products-service/src/Infraestructure/Databases/Factories/CategoryFactory.php' => 
  array (
    0 => 
    PHPStan\Dependency\ExportedNode\ExportedClassNode::__set_state(array(
       'name' => 'VinciarelliFranco\\ProductService\\Infraestructure\\Databases\\Factories\\CategoryFactory',
       'phpDoc' => 
      PHPStan\Dependency\ExportedNode\ExportedPhpDocNode::__set_state(array(
         'phpDocString' => '/**
 * @extends \\Illuminate\\Database\\Eloquent\\Factories\\Factory<\\App\\Models\\Model>
 */',
         'namespace' => 'VinciarelliFranco\\ProductService\\Infraestructure\\Databases\\Factories',
         'uses' => 
        array (
          'faker' => 'Faker\\Factory',
          'factory' => 'Illuminate\\Database\\Eloquent\\Factories\\Factory',
          'category' => 'VinciarelliFranco\\ProductService\\Domain\\Category',
        ),
         'constUses' => 
        array (
        ),
      )),
       'abstract' => false,
       'final' => false,
       'extends' => 'Illuminate\\Database\\Eloquent\\Factories\\Factory',
       'implements' => 
      array (
      ),
       'usedTraits' => 
      array (
      ),
       'traitUseAdaptations' => 
      array (
      ),
       'statements' => 
      array (
        0 => 
        PHPStan\Dependency\ExportedNode\ExportedPropertiesNode::__set_state(array(
           'names' => 
          array (
            0 => 'model',
          ),
           'phpDoc' => 
          PHPStan\Dependency\ExportedNode\ExportedPhpDocNode::__set_state(array(
             'phpDocString' => '/**
     * The name of the factory\'s corresponding model.
     *
     * @var string
     */',
             'namespace' => 'VinciarelliFranco\\ProductService\\Infraestructure\\Databases\\Factories',
             'uses' => 
            array (
              'faker' => 'Faker\\Factory',
              'factory' => 'Illuminate\\Database\\Eloquent\\Factories\\Factory',
              'category' => 'VinciarelliFranco\\ProductService\\Domain\\Category',
            ),
             'constUses' => 
            array (
            ),
          )),
           'type' => NULL,
           'public' => false,
           'private' => false,
           'static' => false,
           'readonly' => false,
           'attributes' => 
          array (
          ),
        )),
        1 => 
        PHPStan\Dependency\ExportedNode\ExportedMethodNode::__set_state(array(
           'name' => 'definition',
           'phpDoc' => 
          PHPStan\Dependency\ExportedNode\ExportedPhpDocNode::__set_state(array(
             'phpDocString' => '/**
     * Define the model\'s default state.
     *
     * @return array<string, mixed>
     */',
             'namespace' => 'VinciarelliFranco\\ProductService\\Infraestructure\\Databases\\Factories',
             'uses' => 
            array (
              'faker' => 'Faker\\Factory',
              'factory' => 'Illuminate\\Database\\Eloquent\\Factories\\Factory',
              'category' => 'VinciarelliFranco\\ProductService\\Domain\\Category',
            ),
             'constUses' => 
            array (
            ),
          )),
           'byRef' => false,
           'public' => true,
           'private' => false,
           'abstract' => false,
           'final' => false,
           'static' => false,
           'returnType' => NULL,
           'parameters' => 
          array (
          ),
           'attributes' => 
          array (
          ),
        )),
      ),
       'attributes' => 
      array (
      ),
    )),
  ),
  '/var/www/html/packages/products-service/src/Infraestructure/Databases/Factories/ProductContractFactory.php' => 
  array (
    0 => 
    PHPStan\Dependency\ExportedNode\ExportedClassNode::__set_state(array(
       'name' => 'VinciarelliFranco\\ProductService\\Infraestructure\\Databases\\Factories\\ProductContractFactory',
       'phpDoc' => 
      PHPStan\Dependency\ExportedNode\ExportedPhpDocNode::__set_state(array(
         'phpDocString' => '/**
 * @extends \\Illuminate\\Database\\Eloquent\\Factories\\Factory<\\App\\Models\\Model>
 */',
         'namespace' => 'VinciarelliFranco\\ProductService\\Infraestructure\\Databases\\Factories',
         'uses' => 
        array (
          'faker' => 'Faker\\Factory',
          'factory' => 'Illuminate\\Database\\Eloquent\\Factories\\Factory',
          'productcontract' => 'VinciarelliFranco\\ProductService\\Domain\\ProductContract',
        ),
         'constUses' => 
        array (
        ),
      )),
       'abstract' => false,
       'final' => false,
       'extends' => 'Illuminate\\Database\\Eloquent\\Factories\\Factory',
       'implements' => 
      array (
      ),
       'usedTraits' => 
      array (
      ),
       'traitUseAdaptations' => 
      array (
      ),
       'statements' => 
      array (
        0 => 
        PHPStan\Dependency\ExportedNode\ExportedPropertiesNode::__set_state(array(
           'names' => 
          array (
            0 => 'model',
          ),
           'phpDoc' => 
          PHPStan\Dependency\ExportedNode\ExportedPhpDocNode::__set_state(array(
             'phpDocString' => '/**
     * The name of the factory\'s corresponding model.
     *
     * @var string
     */',
             'namespace' => 'VinciarelliFranco\\ProductService\\Infraestructure\\Databases\\Factories',
             'uses' => 
            array (
              'faker' => 'Faker\\Factory',
              'factory' => 'Illuminate\\Database\\Eloquent\\Factories\\Factory',
              'productcontract' => 'VinciarelliFranco\\ProductService\\Domain\\ProductContract',
            ),
             'constUses' => 
            array (
            ),
          )),
           'type' => NULL,
           'public' => false,
           'private' => false,
           'static' => false,
           'readonly' => false,
           'attributes' => 
          array (
          ),
        )),
        1 => 
        PHPStan\Dependency\ExportedNode\ExportedMethodNode::__set_state(array(
           'name' => 'definition',
           'phpDoc' => 
          PHPStan\Dependency\ExportedNode\ExportedPhpDocNode::__set_state(array(
             'phpDocString' => '/**
     * Define the model\'s default state.
     *
     * @return array<string, mixed>
     */',
             'namespace' => 'VinciarelliFranco\\ProductService\\Infraestructure\\Databases\\Factories',
             'uses' => 
            array (
              'faker' => 'Faker\\Factory',
              'factory' => 'Illuminate\\Database\\Eloquent\\Factories\\Factory',
              'productcontract' => 'VinciarelliFranco\\ProductService\\Domain\\ProductContract',
            ),
             'constUses' => 
            array (
            ),
          )),
           'byRef' => false,
           'public' => true,
           'private' => false,
           'abstract' => false,
           'final' => false,
           'static' => false,
           'returnType' => NULL,
           'parameters' => 
          array (
          ),
           'attributes' => 
          array (
          ),
        )),
      ),
       'attributes' => 
      array (
      ),
    )),
  ),
  '/var/www/html/packages/products-service/src/Infraestructure/Databases/Factories/ProductFactory.php' => 
  array (
    0 => 
    PHPStan\Dependency\ExportedNode\ExportedClassNode::__set_state(array(
       'name' => 'VinciarelliFranco\\ProductService\\Infraestructure\\Databases\\Factories\\ProductFactory',
       'phpDoc' => 
      PHPStan\Dependency\ExportedNode\ExportedPhpDocNode::__set_state(array(
         'phpDocString' => '/**
 * @extends \\Illuminate\\Database\\Eloquent\\Factories\\Factory<\\App\\Models\\Model>
 */',
         'namespace' => 'VinciarelliFranco\\ProductService\\Infraestructure\\Databases\\Factories',
         'uses' => 
        array (
          'faker' => 'Faker\\Factory',
          'factory' => 'Illuminate\\Database\\Eloquent\\Factories\\Factory',
          'product' => 'VinciarelliFranco\\ProductService\\Domain\\Product',
        ),
         'constUses' => 
        array (
        ),
      )),
       'abstract' => false,
       'final' => false,
       'extends' => 'Illuminate\\Database\\Eloquent\\Factories\\Factory',
       'implements' => 
      array (
      ),
       'usedTraits' => 
      array (
      ),
       'traitUseAdaptations' => 
      array (
      ),
       'statements' => 
      array (
        0 => 
        PHPStan\Dependency\ExportedNode\ExportedPropertiesNode::__set_state(array(
           'names' => 
          array (
            0 => 'model',
          ),
           'phpDoc' => 
          PHPStan\Dependency\ExportedNode\ExportedPhpDocNode::__set_state(array(
             'phpDocString' => '/**
     * The name of the factory\'s corresponding model.
     *
     * @var string
     */',
             'namespace' => 'VinciarelliFranco\\ProductService\\Infraestructure\\Databases\\Factories',
             'uses' => 
            array (
              'faker' => 'Faker\\Factory',
              'factory' => 'Illuminate\\Database\\Eloquent\\Factories\\Factory',
              'product' => 'VinciarelliFranco\\ProductService\\Domain\\Product',
            ),
             'constUses' => 
            array (
            ),
          )),
           'type' => NULL,
           'public' => false,
           'private' => false,
           'static' => false,
           'readonly' => false,
           'attributes' => 
          array (
          ),
        )),
        1 => 
        PHPStan\Dependency\ExportedNode\ExportedMethodNode::__set_state(array(
           'name' => 'definition',
           'phpDoc' => 
          PHPStan\Dependency\ExportedNode\ExportedPhpDocNode::__set_state(array(
             'phpDocString' => '/**
     * Define the model\'s default state.
     *
     * @return array<string, mixed>
     */',
             'namespace' => 'VinciarelliFranco\\ProductService\\Infraestructure\\Databases\\Factories',
             'uses' => 
            array (
              'faker' => 'Faker\\Factory',
              'factory' => 'Illuminate\\Database\\Eloquent\\Factories\\Factory',
              'product' => 'VinciarelliFranco\\ProductService\\Domain\\Product',
            ),
             'constUses' => 
            array (
            ),
          )),
           'byRef' => false,
           'public' => true,
           'private' => false,
           'abstract' => false,
           'final' => false,
           'static' => false,
           'returnType' => NULL,
           'parameters' => 
          array (
          ),
           'attributes' => 
          array (
          ),
        )),
      ),
       'attributes' => 
      array (
      ),
    )),
  ),
  '/var/www/html/packages/products-service/src/Infraestructure/Databases/Factories/ProductRatingFactory.php' => 
  array (
    0 => 
    PHPStan\Dependency\ExportedNode\ExportedClassNode::__set_state(array(
       'name' => 'VinciarelliFranco\\ProductService\\Infraestructure\\Databases\\Factories\\ProductRatingFactory',
       'phpDoc' => 
      PHPStan\Dependency\ExportedNode\ExportedPhpDocNode::__set_state(array(
         'phpDocString' => '/**
 * @extends \\Illuminate\\Database\\Eloquent\\Factories\\Factory<\\App\\Models\\Model>
 */',
         'namespace' => 'VinciarelliFranco\\ProductService\\Infraestructure\\Databases\\Factories',
         'uses' => 
        array (
          'faker' => 'Faker\\Factory',
          'factory' => 'Illuminate\\Database\\Eloquent\\Factories\\Factory',
          'productrating' => 'VinciarelliFranco\\ProductService\\Domain\\ProductRating',
        ),
         'constUses' => 
        array (
        ),
      )),
       'abstract' => false,
       'final' => false,
       'extends' => 'Illuminate\\Database\\Eloquent\\Factories\\Factory',
       'implements' => 
      array (
      ),
       'usedTraits' => 
      array (
      ),
       'traitUseAdaptations' => 
      array (
      ),
       'statements' => 
      array (
        0 => 
        PHPStan\Dependency\ExportedNode\ExportedPropertiesNode::__set_state(array(
           'names' => 
          array (
            0 => 'model',
          ),
           'phpDoc' => 
          PHPStan\Dependency\ExportedNode\ExportedPhpDocNode::__set_state(array(
             'phpDocString' => '/**
     * The name of the factory\'s corresponding model.
     *
     * @var string
     */',
             'namespace' => 'VinciarelliFranco\\ProductService\\Infraestructure\\Databases\\Factories',
             'uses' => 
            array (
              'faker' => 'Faker\\Factory',
              'factory' => 'Illuminate\\Database\\Eloquent\\Factories\\Factory',
              'productrating' => 'VinciarelliFranco\\ProductService\\Domain\\ProductRating',
            ),
             'constUses' => 
            array (
            ),
          )),
           'type' => NULL,
           'public' => false,
           'private' => false,
           'static' => false,
           'readonly' => false,
           'attributes' => 
          array (
          ),
        )),
        1 => 
        PHPStan\Dependency\ExportedNode\ExportedMethodNode::__set_state(array(
           'name' => 'definition',
           'phpDoc' => 
          PHPStan\Dependency\ExportedNode\ExportedPhpDocNode::__set_state(array(
             'phpDocString' => '/**
     * Define the model\'s default state.
     *
     * @return array<string, mixed>
     */',
             'namespace' => 'VinciarelliFranco\\ProductService\\Infraestructure\\Databases\\Factories',
             'uses' => 
            array (
              'faker' => 'Faker\\Factory',
              'factory' => 'Illuminate\\Database\\Eloquent\\Factories\\Factory',
              'productrating' => 'VinciarelliFranco\\ProductService\\Domain\\ProductRating',
            ),
             'constUses' => 
            array (
            ),
          )),
           'byRef' => false,
           'public' => true,
           'private' => false,
           'abstract' => false,
           'final' => false,
           'static' => false,
           'returnType' => NULL,
           'parameters' => 
          array (
          ),
           'attributes' => 
          array (
          ),
        )),
      ),
       'attributes' => 
      array (
      ),
    )),
  ),
  '/var/www/html/packages/products-service/src/Infraestructure/Databases/Migrations/2022_12_04_220659_create_categories_table.php' => 
  array (
    0 => 
    PHPStan\Dependency\ExportedNode\ExportedClassNode::__set_state(array(
       'name' => 'CreateCategoriesTable',
       'phpDoc' => NULL,
       'abstract' => false,
       'final' => false,
       'extends' => 'Illuminate\\Database\\Migrations\\Migration',
       'implements' => 
      array (
      ),
       'usedTraits' => 
      array (
      ),
       'traitUseAdaptations' => 
      array (
      ),
       'statements' => 
      array (
        0 => 
        PHPStan\Dependency\ExportedNode\ExportedMethodNode::__set_state(array(
           'name' => 'up',
           'phpDoc' => 
          PHPStan\Dependency\ExportedNode\ExportedPhpDocNode::__set_state(array(
             'phpDocString' => '/**
     * Run the migrations.
     *
     * @return void
     */',
             'namespace' => NULL,
             'uses' => 
            array (
              'migration' => 'Illuminate\\Database\\Migrations\\Migration',
              'blueprint' => 'Illuminate\\Database\\Schema\\Blueprint',
              'schema' => 'Illuminate\\Support\\Facades\\Schema',
            ),
             'constUses' => 
            array (
            ),
          )),
           'byRef' => false,
           'public' => true,
           'private' => false,
           'abstract' => false,
           'final' => false,
           'static' => false,
           'returnType' => NULL,
           'parameters' => 
          array (
          ),
           'attributes' => 
          array (
          ),
        )),
        1 => 
        PHPStan\Dependency\ExportedNode\ExportedMethodNode::__set_state(array(
           'name' => 'down',
           'phpDoc' => 
          PHPStan\Dependency\ExportedNode\ExportedPhpDocNode::__set_state(array(
             'phpDocString' => '/**
     * Reverse the migrations.
     *
     * @return void
     */',
             'namespace' => NULL,
             'uses' => 
            array (
              'migration' => 'Illuminate\\Database\\Migrations\\Migration',
              'blueprint' => 'Illuminate\\Database\\Schema\\Blueprint',
              'schema' => 'Illuminate\\Support\\Facades\\Schema',
            ),
             'constUses' => 
            array (
            ),
          )),
           'byRef' => false,
           'public' => true,
           'private' => false,
           'abstract' => false,
           'final' => false,
           'static' => false,
           'returnType' => NULL,
           'parameters' => 
          array (
          ),
           'attributes' => 
          array (
          ),
        )),
      ),
       'attributes' => 
      array (
      ),
    )),
  ),
  '/var/www/html/packages/products-service/src/Infraestructure/Databases/Migrations/2022_12_04_220670_create_products_table.php' => 
  array (
    0 => 
    PHPStan\Dependency\ExportedNode\ExportedClassNode::__set_state(array(
       'name' => 'CreateProductsTable',
       'phpDoc' => NULL,
       'abstract' => false,
       'final' => false,
       'extends' => 'Illuminate\\Database\\Migrations\\Migration',
       'implements' => 
      array (
      ),
       'usedTraits' => 
      array (
      ),
       'traitUseAdaptations' => 
      array (
      ),
       'statements' => 
      array (
        0 => 
        PHPStan\Dependency\ExportedNode\ExportedMethodNode::__set_state(array(
           'name' => 'up',
           'phpDoc' => 
          PHPStan\Dependency\ExportedNode\ExportedPhpDocNode::__set_state(array(
             'phpDocString' => '/**
     * Run the migrations.
     *
     * @return void
     */',
             'namespace' => NULL,
             'uses' => 
            array (
              'migration' => 'Illuminate\\Database\\Migrations\\Migration',
              'blueprint' => 'Illuminate\\Database\\Schema\\Blueprint',
              'schema' => 'Illuminate\\Support\\Facades\\Schema',
            ),
             'constUses' => 
            array (
            ),
          )),
           'byRef' => false,
           'public' => true,
           'private' => false,
           'abstract' => false,
           'final' => false,
           'static' => false,
           'returnType' => NULL,
           'parameters' => 
          array (
          ),
           'attributes' => 
          array (
          ),
        )),
        1 => 
        PHPStan\Dependency\ExportedNode\ExportedMethodNode::__set_state(array(
           'name' => 'down',
           'phpDoc' => 
          PHPStan\Dependency\ExportedNode\ExportedPhpDocNode::__set_state(array(
             'phpDocString' => '/**
     * Reverse the migrations.
     *
     * @return void
     */',
             'namespace' => NULL,
             'uses' => 
            array (
              'migration' => 'Illuminate\\Database\\Migrations\\Migration',
              'blueprint' => 'Illuminate\\Database\\Schema\\Blueprint',
              'schema' => 'Illuminate\\Support\\Facades\\Schema',
            ),
             'constUses' => 
            array (
            ),
          )),
           'byRef' => false,
           'public' => true,
           'private' => false,
           'abstract' => false,
           'final' => false,
           'static' => false,
           'returnType' => NULL,
           'parameters' => 
          array (
          ),
           'attributes' => 
          array (
          ),
        )),
      ),
       'attributes' => 
      array (
      ),
    )),
  ),
  '/var/www/html/packages/products-service/src/Infraestructure/Databases/Migrations/2022_12_05_183917_create_product_ratings_table.php' => 
  array (
    0 => 
    PHPStan\Dependency\ExportedNode\ExportedClassNode::__set_state(array(
       'name' => 'CreateProductRatingsTable',
       'phpDoc' => NULL,
       'abstract' => false,
       'final' => false,
       'extends' => 'Illuminate\\Database\\Migrations\\Migration',
       'implements' => 
      array (
      ),
       'usedTraits' => 
      array (
      ),
       'traitUseAdaptations' => 
      array (
      ),
       'statements' => 
      array (
        0 => 
        PHPStan\Dependency\ExportedNode\ExportedMethodNode::__set_state(array(
           'name' => 'up',
           'phpDoc' => 
          PHPStan\Dependency\ExportedNode\ExportedPhpDocNode::__set_state(array(
             'phpDocString' => '/**
     * Run the migrations.
     *
     * @return void
     */',
             'namespace' => NULL,
             'uses' => 
            array (
              'migration' => 'Illuminate\\Database\\Migrations\\Migration',
              'blueprint' => 'Illuminate\\Database\\Schema\\Blueprint',
              'schema' => 'Illuminate\\Support\\Facades\\Schema',
            ),
             'constUses' => 
            array (
            ),
          )),
           'byRef' => false,
           'public' => true,
           'private' => false,
           'abstract' => false,
           'final' => false,
           'static' => false,
           'returnType' => NULL,
           'parameters' => 
          array (
          ),
           'attributes' => 
          array (
          ),
        )),
        1 => 
        PHPStan\Dependency\ExportedNode\ExportedMethodNode::__set_state(array(
           'name' => 'down',
           'phpDoc' => 
          PHPStan\Dependency\ExportedNode\ExportedPhpDocNode::__set_state(array(
             'phpDocString' => '/**
     * Reverse the migrations.
     *
     * @return void
     */',
             'namespace' => NULL,
             'uses' => 
            array (
              'migration' => 'Illuminate\\Database\\Migrations\\Migration',
              'blueprint' => 'Illuminate\\Database\\Schema\\Blueprint',
              'schema' => 'Illuminate\\Support\\Facades\\Schema',
            ),
             'constUses' => 
            array (
            ),
          )),
           'byRef' => false,
           'public' => true,
           'private' => false,
           'abstract' => false,
           'final' => false,
           'static' => false,
           'returnType' => NULL,
           'parameters' => 
          array (
          ),
           'attributes' => 
          array (
          ),
        )),
      ),
       'attributes' => 
      array (
      ),
    )),
  ),
  '/var/www/html/packages/products-service/src/Infraestructure/Databases/Migrations/2022_12_05_184128_create_product_contracts_table.php' => 
  array (
    0 => 
    PHPStan\Dependency\ExportedNode\ExportedClassNode::__set_state(array(
       'name' => 'CreateProductContractsTable',
       'phpDoc' => NULL,
       'abstract' => false,
       'final' => false,
       'extends' => 'Illuminate\\Database\\Migrations\\Migration',
       'implements' => 
      array (
      ),
       'usedTraits' => 
      array (
      ),
       'traitUseAdaptations' => 
      array (
      ),
       'statements' => 
      array (
        0 => 
        PHPStan\Dependency\ExportedNode\ExportedMethodNode::__set_state(array(
           'name' => 'up',
           'phpDoc' => 
          PHPStan\Dependency\ExportedNode\ExportedPhpDocNode::__set_state(array(
             'phpDocString' => '/**
     * Run the migrations.
     *
     * @return void
     */',
             'namespace' => NULL,
             'uses' => 
            array (
              'migration' => 'Illuminate\\Database\\Migrations\\Migration',
              'blueprint' => 'Illuminate\\Database\\Schema\\Blueprint',
              'schema' => 'Illuminate\\Support\\Facades\\Schema',
            ),
             'constUses' => 
            array (
            ),
          )),
           'byRef' => false,
           'public' => true,
           'private' => false,
           'abstract' => false,
           'final' => false,
           'static' => false,
           'returnType' => NULL,
           'parameters' => 
          array (
          ),
           'attributes' => 
          array (
          ),
        )),
        1 => 
        PHPStan\Dependency\ExportedNode\ExportedMethodNode::__set_state(array(
           'name' => 'down',
           'phpDoc' => 
          PHPStan\Dependency\ExportedNode\ExportedPhpDocNode::__set_state(array(
             'phpDocString' => '/**
     * Reverse the migrations.
     *
     * @return void
     */',
             'namespace' => NULL,
             'uses' => 
            array (
              'migration' => 'Illuminate\\Database\\Migrations\\Migration',
              'blueprint' => 'Illuminate\\Database\\Schema\\Blueprint',
              'schema' => 'Illuminate\\Support\\Facades\\Schema',
            ),
             'constUses' => 
            array (
            ),
          )),
           'byRef' => false,
           'public' => true,
           'private' => false,
           'abstract' => false,
           'final' => false,
           'static' => false,
           'returnType' => NULL,
           'parameters' => 
          array (
          ),
           'attributes' => 
          array (
          ),
        )),
      ),
       'attributes' => 
      array (
      ),
    )),
  ),
  '/var/www/html/packages/products-service/src/Infraestructure/Databases/Seeds/ProductsServiceSeeder.php' => 
  array (
    0 => 
    PHPStan\Dependency\ExportedNode\ExportedClassNode::__set_state(array(
       'name' => 'Database\\Seeders\\ProductsServiceSeeder',
       'phpDoc' => NULL,
       'abstract' => false,
       'final' => false,
       'extends' => 'Illuminate\\Database\\Seeder',
       'implements' => 
      array (
      ),
       'usedTraits' => 
      array (
      ),
       'traitUseAdaptations' => 
      array (
      ),
       'statements' => 
      array (
        0 => 
        PHPStan\Dependency\ExportedNode\ExportedMethodNode::__set_state(array(
           'name' => 'run',
           'phpDoc' => NULL,
           'byRef' => false,
           'public' => true,
           'private' => false,
           'abstract' => false,
           'final' => false,
           'static' => false,
           'returnType' => NULL,
           'parameters' => 
          array (
          ),
           'attributes' => 
          array (
          ),
        )),
      ),
       'attributes' => 
      array (
      ),
    )),
  ),
  '/var/www/html/packages/products-service/src/Providers/Http/Controllers/Controller.php' => 
  array (
    0 => 
    PHPStan\Dependency\ExportedNode\ExportedClassNode::__set_state(array(
       'name' => 'VinciarelliFranco\\ProductService\\Providers\\Http\\Controllers\\Controller',
       'phpDoc' => NULL,
       'abstract' => false,
       'final' => false,
       'extends' => 'Illuminate\\Routing\\Controller',
       'implements' => 
      array (
      ),
       'usedTraits' => 
      array (
        0 => 'Illuminate\\Foundation\\Auth\\Access\\AuthorizesRequests',
        1 => 'Illuminate\\Foundation\\Bus\\DispatchesJobs',
        2 => 'Illuminate\\Foundation\\Validation\\ValidatesRequests',
      ),
       'traitUseAdaptations' => 
      array (
      ),
       'statements' => 
      array (
      ),
       'attributes' => 
      array (
      ),
    )),
  ),
  '/var/www/html/packages/products-service/src/Providers/Http/Controllers/ProductContractTypesListController.php' => 
  array (
    0 => 
    PHPStan\Dependency\ExportedNode\ExportedClassNode::__set_state(array(
       'name' => 'VinciarelliFranco\\ProductService\\Providers\\Http\\Controllers\\ProductContractTypesListController',
       'phpDoc' => NULL,
       'abstract' => false,
       'final' => false,
       'extends' => 'VinciarelliFranco\\ProductService\\Providers\\Http\\Controllers\\Controller',
       'implements' => 
      array (
      ),
       'usedTraits' => 
      array (
      ),
       'traitUseAdaptations' => 
      array (
      ),
       'statements' => 
      array (
        0 => 
        PHPStan\Dependency\ExportedNode\ExportedMethodNode::__set_state(array(
           'name' => '__invoke',
           'phpDoc' => NULL,
           'byRef' => false,
           'public' => true,
           'private' => false,
           'abstract' => false,
           'final' => false,
           'static' => false,
           'returnType' => NULL,
           'parameters' => 
          array (
            0 => 
            PHPStan\Dependency\ExportedNode\ExportedParameterNode::__set_state(array(
               'name' => 'request',
               'type' => 'Illuminate\\Support\\Facades\\Request',
               'byRef' => false,
               'variadic' => false,
               'hasDefault' => false,
               'attributes' => 
              array (
              ),
            )),
          ),
           'attributes' => 
          array (
          ),
        )),
      ),
       'attributes' => 
      array (
      ),
    )),
  ),
  '/var/www/html/packages/products-service/src/Providers/Http/Controllers/ProductFilterOrderListController.php' => 
  array (
    0 => 
    PHPStan\Dependency\ExportedNode\ExportedClassNode::__set_state(array(
       'name' => 'VinciarelliFranco\\ProductService\\Providers\\Http\\Controllers\\ProductFilterOrderListController',
       'phpDoc' => NULL,
       'abstract' => false,
       'final' => false,
       'extends' => 'VinciarelliFranco\\ProductService\\Providers\\Http\\Controllers\\Controller',
       'implements' => 
      array (
      ),
       'usedTraits' => 
      array (
      ),
       'traitUseAdaptations' => 
      array (
      ),
       'statements' => 
      array (
        0 => 
        PHPStan\Dependency\ExportedNode\ExportedMethodNode::__set_state(array(
           'name' => '__invoke',
           'phpDoc' => NULL,
           'byRef' => false,
           'public' => true,
           'private' => false,
           'abstract' => false,
           'final' => false,
           'static' => false,
           'returnType' => NULL,
           'parameters' => 
          array (
            0 => 
            PHPStan\Dependency\ExportedNode\ExportedParameterNode::__set_state(array(
               'name' => 'request',
               'type' => 'Illuminate\\Support\\Facades\\Request',
               'byRef' => false,
               'variadic' => false,
               'hasDefault' => false,
               'attributes' => 
              array (
              ),
            )),
          ),
           'attributes' => 
          array (
          ),
        )),
      ),
       'attributes' => 
      array (
      ),
    )),
  ),
  '/var/www/html/packages/products-service/src/Providers/Http/Controllers/ProductPrinceRangeListController.php' => 
  array (
    0 => 
    PHPStan\Dependency\ExportedNode\ExportedClassNode::__set_state(array(
       'name' => 'VinciarelliFranco\\ProductService\\Providers\\Http\\Controllers\\ProductPrinceRangeListController',
       'phpDoc' => NULL,
       'abstract' => false,
       'final' => false,
       'extends' => 'VinciarelliFranco\\ProductService\\Providers\\Http\\Controllers\\Controller',
       'implements' => 
      array (
      ),
       'usedTraits' => 
      array (
      ),
       'traitUseAdaptations' => 
      array (
      ),
       'statements' => 
      array (
        0 => 
        PHPStan\Dependency\ExportedNode\ExportedMethodNode::__set_state(array(
           'name' => '__invoke',
           'phpDoc' => NULL,
           'byRef' => false,
           'public' => true,
           'private' => false,
           'abstract' => false,
           'final' => false,
           'static' => false,
           'returnType' => NULL,
           'parameters' => 
          array (
            0 => 
            PHPStan\Dependency\ExportedNode\ExportedParameterNode::__set_state(array(
               'name' => 'request',
               'type' => 'Illuminate\\Support\\Facades\\Request',
               'byRef' => false,
               'variadic' => false,
               'hasDefault' => false,
               'attributes' => 
              array (
              ),
            )),
          ),
           'attributes' => 
          array (
          ),
        )),
      ),
       'attributes' => 
      array (
      ),
    )),
  ),
  '/var/www/html/packages/products-service/src/Providers/Http/Controllers/ProductSearchController.php' => 
  array (
    0 => 
    PHPStan\Dependency\ExportedNode\ExportedClassNode::__set_state(array(
       'name' => 'VinciarelliFranco\\ProductService\\Providers\\Http\\Controllers\\ProductSearchController',
       'phpDoc' => NULL,
       'abstract' => false,
       'final' => false,
       'extends' => 'VinciarelliFranco\\ProductService\\Providers\\Http\\Controllers\\Controller',
       'implements' => 
      array (
      ),
       'usedTraits' => 
      array (
      ),
       'traitUseAdaptations' => 
      array (
      ),
       'statements' => 
      array (
        0 => 
        PHPStan\Dependency\ExportedNode\ExportedMethodNode::__set_state(array(
           'name' => '__invoke',
           'phpDoc' => NULL,
           'byRef' => false,
           'public' => true,
           'private' => false,
           'abstract' => false,
           'final' => false,
           'static' => false,
           'returnType' => NULL,
           'parameters' => 
          array (
            0 => 
            PHPStan\Dependency\ExportedNode\ExportedParameterNode::__set_state(array(
               'name' => 'request',
               'type' => 'Illuminate\\Support\\Facades\\Request',
               'byRef' => false,
               'variadic' => false,
               'hasDefault' => false,
               'attributes' => 
              array (
              ),
            )),
          ),
           'attributes' => 
          array (
          ),
        )),
      ),
       'attributes' => 
      array (
      ),
    )),
  ),
  '/var/www/html/packages/products-service/src/Providers/Middlewares/AfterCacheSearchMiddleware.php' => 
  array (
    0 => 
    PHPStan\Dependency\ExportedNode\ExportedClassNode::__set_state(array(
       'name' => 'VinciarelliFranco\\ProductService\\Providers\\Middlewares\\AfterCacheSearchMiddleware',
       'phpDoc' => NULL,
       'abstract' => false,
       'final' => false,
       'extends' => NULL,
       'implements' => 
      array (
      ),
       'usedTraits' => 
      array (
      ),
       'traitUseAdaptations' => 
      array (
      ),
       'statements' => 
      array (
        0 => 
        PHPStan\Dependency\ExportedNode\ExportedMethodNode::__set_state(array(
           'name' => 'handle',
           'phpDoc' => NULL,
           'byRef' => false,
           'public' => true,
           'private' => false,
           'abstract' => false,
           'final' => false,
           'static' => false,
           'returnType' => NULL,
           'parameters' => 
          array (
            0 => 
            PHPStan\Dependency\ExportedNode\ExportedParameterNode::__set_state(array(
               'name' => 'request',
               'type' => NULL,
               'byRef' => false,
               'variadic' => false,
               'hasDefault' => false,
               'attributes' => 
              array (
              ),
            )),
            1 => 
            PHPStan\Dependency\ExportedNode\ExportedParameterNode::__set_state(array(
               'name' => 'next',
               'type' => 'Closure',
               'byRef' => false,
               'variadic' => false,
               'hasDefault' => false,
               'attributes' => 
              array (
              ),
            )),
          ),
           'attributes' => 
          array (
          ),
        )),
      ),
       'attributes' => 
      array (
      ),
    )),
  ),
  '/var/www/html/packages/products-service/src/Providers/Middlewares/BeforeCacheSearchMiddleware.php' => 
  array (
    0 => 
    PHPStan\Dependency\ExportedNode\ExportedClassNode::__set_state(array(
       'name' => 'VinciarelliFranco\\ProductService\\Providers\\Middlewares\\BeforeCacheSearchMiddleware',
       'phpDoc' => NULL,
       'abstract' => false,
       'final' => false,
       'extends' => NULL,
       'implements' => 
      array (
      ),
       'usedTraits' => 
      array (
      ),
       'traitUseAdaptations' => 
      array (
      ),
       'statements' => 
      array (
        0 => 
        PHPStan\Dependency\ExportedNode\ExportedMethodNode::__set_state(array(
           'name' => 'handle',
           'phpDoc' => NULL,
           'byRef' => false,
           'public' => true,
           'private' => false,
           'abstract' => false,
           'final' => false,
           'static' => false,
           'returnType' => NULL,
           'parameters' => 
          array (
            0 => 
            PHPStan\Dependency\ExportedNode\ExportedParameterNode::__set_state(array(
               'name' => 'request',
               'type' => NULL,
               'byRef' => false,
               'variadic' => false,
               'hasDefault' => false,
               'attributes' => 
              array (
              ),
            )),
            1 => 
            PHPStan\Dependency\ExportedNode\ExportedParameterNode::__set_state(array(
               'name' => 'next',
               'type' => 'Closure',
               'byRef' => false,
               'variadic' => false,
               'hasDefault' => false,
               'attributes' => 
              array (
              ),
            )),
          ),
           'attributes' => 
          array (
          ),
        )),
      ),
       'attributes' => 
      array (
      ),
    )),
  ),
  '/var/www/html/packages/products-service/src/Providers/ProductServiceProvider.php' => 
  array (
    0 => 
    PHPStan\Dependency\ExportedNode\ExportedClassNode::__set_state(array(
       'name' => 'VinciarelliFranco\\ProductService\\Providers\\ProductServiceProvider',
       'phpDoc' => NULL,
       'abstract' => false,
       'final' => false,
       'extends' => 'Illuminate\\Support\\ServiceProvider',
       'implements' => 
      array (
      ),
       'usedTraits' => 
      array (
      ),
       'traitUseAdaptations' => 
      array (
      ),
       'statements' => 
      array (
        0 => 
        PHPStan\Dependency\ExportedNode\ExportedMethodNode::__set_state(array(
           'name' => 'register',
           'phpDoc' => NULL,
           'byRef' => false,
           'public' => true,
           'private' => false,
           'abstract' => false,
           'final' => false,
           'static' => false,
           'returnType' => NULL,
           'parameters' => 
          array (
          ),
           'attributes' => 
          array (
          ),
        )),
        1 => 
        PHPStan\Dependency\ExportedNode\ExportedMethodNode::__set_state(array(
           'name' => 'boot',
           'phpDoc' => NULL,
           'byRef' => false,
           'public' => true,
           'private' => false,
           'abstract' => false,
           'final' => false,
           'static' => false,
           'returnType' => NULL,
           'parameters' => 
          array (
          ),
           'attributes' => 
          array (
          ),
        )),
        2 => 
        PHPStan\Dependency\ExportedNode\ExportedMethodNode::__set_state(array(
           'name' => 'registerRoutes',
           'phpDoc' => NULL,
           'byRef' => false,
           'public' => false,
           'private' => false,
           'abstract' => false,
           'final' => false,
           'static' => false,
           'returnType' => NULL,
           'parameters' => 
          array (
          ),
           'attributes' => 
          array (
          ),
        )),
        3 => 
        PHPStan\Dependency\ExportedNode\ExportedMethodNode::__set_state(array(
           'name' => 'routeConfiguration',
           'phpDoc' => NULL,
           'byRef' => false,
           'public' => false,
           'private' => false,
           'abstract' => false,
           'final' => false,
           'static' => false,
           'returnType' => NULL,
           'parameters' => 
          array (
          ),
           'attributes' => 
          array (
          ),
        )),
        4 => 
        PHPStan\Dependency\ExportedNode\ExportedMethodNode::__set_state(array(
           'name' => 'publishSeeders',
           'phpDoc' => NULL,
           'byRef' => false,
           'public' => false,
           'private' => false,
           'abstract' => false,
           'final' => false,
           'static' => false,
           'returnType' => NULL,
           'parameters' => 
          array (
          ),
           'attributes' => 
          array (
          ),
        )),
      ),
       'attributes' => 
      array (
      ),
    )),
  ),
  '/var/www/html/packages/products-service/src/Share/IDDDCommand.php' => 
  array (
    0 => 
    PHPStan\Dependency\ExportedNode\ExportedInterfaceNode::__set_state(array(
       'name' => 'VinciarelliFranco\\ProductService\\Share\\IDDDCommand',
       'phpDoc' => NULL,
       'extends' => 
      array (
      ),
       'statements' => 
      array (
      ),
    )),
  ),
  '/var/www/html/packages/products-service/src/Share/IDDDCommandHandlers.php' => 
  array (
    0 => 
    PHPStan\Dependency\ExportedNode\ExportedInterfaceNode::__set_state(array(
       'name' => 'VinciarelliFranco\\ProductService\\Share\\IDDDCommandHandlers',
       'phpDoc' => NULL,
       'extends' => 
      array (
      ),
       'statements' => 
      array (
        0 => 
        PHPStan\Dependency\ExportedNode\ExportedMethodNode::__set_state(array(
           'name' => 'handle',
           'phpDoc' => NULL,
           'byRef' => false,
           'public' => true,
           'private' => false,
           'abstract' => false,
           'final' => false,
           'static' => true,
           'returnType' => NULL,
           'parameters' => 
          array (
            0 => 
            PHPStan\Dependency\ExportedNode\ExportedParameterNode::__set_state(array(
               'name' => 'command',
               'type' => NULL,
               'byRef' => false,
               'variadic' => false,
               'hasDefault' => false,
               'attributes' => 
              array (
              ),
            )),
          ),
           'attributes' => 
          array (
          ),
        )),
      ),
    )),
  ),
  '/var/www/html/packages/products-service/tests/Feature/ProductSearchTest.php' => 
  array (
    0 => 
    PHPStan\Dependency\ExportedNode\ExportedClassNode::__set_state(array(
       'name' => 'VinciarelliFranco\\ProductService\\Tests\\Feature\\CreatePostTest',
       'phpDoc' => NULL,
       'abstract' => false,
       'final' => false,
       'extends' => 'VinciarelliFranco\\ProductService\\Tests\\TestCase',
       'implements' => 
      array (
      ),
       'usedTraits' => 
      array (
        0 => 'Illuminate\\Foundation\\Testing\\RefreshDatabase',
      ),
       'traitUseAdaptations' => 
      array (
      ),
       'statements' => 
      array (
        0 => 
        PHPStan\Dependency\ExportedNode\ExportedMethodNode::__set_state(array(
           'name' => 'check_route_is_working',
           'phpDoc' => 
          PHPStan\Dependency\ExportedNode\ExportedPhpDocNode::__set_state(array(
             'phpDocString' => '/** @test */',
             'namespace' => 'VinciarelliFranco\\ProductService\\Tests\\Feature',
             'uses' => 
            array (
              'refreshdatabase' => 'Illuminate\\Foundation\\Testing\\RefreshDatabase',
              'testcase' => 'VinciarelliFranco\\ProductService\\Tests\\TestCase',
            ),
             'constUses' => 
            array (
            ),
          )),
           'byRef' => false,
           'public' => true,
           'private' => false,
           'abstract' => false,
           'final' => false,
           'static' => false,
           'returnType' => NULL,
           'parameters' => 
          array (
          ),
           'attributes' => 
          array (
          ),
        )),
      ),
       'attributes' => 
      array (
      ),
    )),
  ),
  '/var/www/html/packages/products-service/tests/TestCase.php' => 
  array (
    0 => 
    PHPStan\Dependency\ExportedNode\ExportedClassNode::__set_state(array(
       'name' => 'VinciarelliFranco\\ProductService\\Tests\\TestCase',
       'phpDoc' => NULL,
       'abstract' => false,
       'final' => false,
       'extends' => 'Orchestra\\Testbench\\TestCase',
       'implements' => 
      array (
      ),
       'usedTraits' => 
      array (
      ),
       'traitUseAdaptations' => 
      array (
      ),
       'statements' => 
      array (
        0 => 
        PHPStan\Dependency\ExportedNode\ExportedMethodNode::__set_state(array(
           'name' => 'setUp',
           'phpDoc' => NULL,
           'byRef' => false,
           'public' => true,
           'private' => false,
           'abstract' => false,
           'final' => false,
           'static' => false,
           'returnType' => 'void',
           'parameters' => 
          array (
          ),
           'attributes' => 
          array (
          ),
        )),
        1 => 
        PHPStan\Dependency\ExportedNode\ExportedMethodNode::__set_state(array(
           'name' => 'getPackageProviders',
           'phpDoc' => NULL,
           'byRef' => false,
           'public' => false,
           'private' => false,
           'abstract' => false,
           'final' => false,
           'static' => false,
           'returnType' => NULL,
           'parameters' => 
          array (
            0 => 
            PHPStan\Dependency\ExportedNode\ExportedParameterNode::__set_state(array(
               'name' => 'app',
               'type' => NULL,
               'byRef' => false,
               'variadic' => false,
               'hasDefault' => false,
               'attributes' => 
              array (
              ),
            )),
          ),
           'attributes' => 
          array (
          ),
        )),
        2 => 
        PHPStan\Dependency\ExportedNode\ExportedMethodNode::__set_state(array(
           'name' => 'getEnvironmentSetUp',
           'phpDoc' => NULL,
           'byRef' => false,
           'public' => false,
           'private' => false,
           'abstract' => false,
           'final' => false,
           'static' => false,
           'returnType' => NULL,
           'parameters' => 
          array (
            0 => 
            PHPStan\Dependency\ExportedNode\ExportedParameterNode::__set_state(array(
               'name' => 'app',
               'type' => NULL,
               'byRef' => false,
               'variadic' => false,
               'hasDefault' => false,
               'attributes' => 
              array (
              ),
            )),
          ),
           'attributes' => 
          array (
          ),
        )),
      ),
       'attributes' => 
      array (
      ),
    )),
  ),
); },
];
