<?php

namespace VinciarelliFranco\ProductService\Application\Service\ProductFiltersList;

use Exception;
use VinciarelliFranco\ProductService\Share\IDDDCommand;

class ProductFiltersListCommand implements IDDDCommand
{
    public static function fromWebRequest(?array $data): ProductFiltersListCommand
    {
        return new self();
    }

    private function __construct()
    {
    }

    public function getAll(): array
    {
        return [];
    }
}
