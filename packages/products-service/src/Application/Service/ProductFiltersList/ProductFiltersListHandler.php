<?php

namespace VinciarelliFranco\ProductService\Application\Service\ProductFiltersList;

use Exception;
use Symfony\Component\HttpKernel\Exception\HttpException;
use VinciarelliFranco\ProductService\Share\IDDDCommandHandlers;

class ProductFiltersListHandler implements IDDDCommandHandlers
{
    public static function handle($command)
    {
        try {
            return [
                'autonomy' => [
                    'asc'   => 'low to hight price autonomy',
                    'desc'  => 'hight to low price autonomy'
                ],
                'price' => [
                    'asc'   => 'low to hight price',
                    'desc'  => 'hight to low price'
                ],
                //and so on
            ];
        } catch (Exception $e) {
            //Log an send error
            throw new HttpException(500, $e->getMessage());
        }
    }
}
