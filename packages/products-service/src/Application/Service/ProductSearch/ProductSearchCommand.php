<?php

namespace VinciarelliFranco\ProductService\Application\Service\ProductSearch;

use Exception;
use Illuminate\Support\Facades\Validator;
use VinciarelliFranco\ProductService\Share\IDDDCommand;

/**
 * Product Search Command
 */
class ProductSearchCommand implements IDDDCommand
{
    protected int $from_record;
    protected int $to_record;
    protected ?float $price_from;
    protected ?float $price_to;
    protected ?int $category_id;
    protected ?int $allow_load;
    protected ?int $allow_hideaway;
    protected ?int $allow_no_license;
    protected int $contract_type;
    protected array $order_by_field;

    const DEFAULT_LIMIT_RECORDSET = 10;
    const DEFAULT_START_RECORDSET = 0;
    const DEFAULT_ORDER_RECORDSET = 'ID_ASC';
    const DEFAULT_CONTRACT_TYPE = 12;

    protected const RULES = [
        'from' => 'numeric',
        'to' => 'numeric',
        'price_from' => 'numeric',
        'price_to' => 'numeric',
        'category' => 'numeric',
        'allow_load' => 'numeric',
        'allow_hideaway' => 'numeric',
        'allow_no_license' => 'numeric',
        'contract_type' => 'numeric',
        'orderby' => 'string'
    ];

    /**
     * Adapt data from Web request to build a command which can be used by the application service 
     * 
     * @param ?array $data
     * @return ProductSearchCommand
     *  
     */
    public static function fromWebRequest(?array $data): ProductSearchCommand
    {
        $validator = Validator::make($data, self::RULES);
        if ($validator->fails()) {
            throw new Exception('Invalid filter data');
            exit();
        }

        //Return a new object
        return new self(
            $data['from'] ?? self::DEFAULT_START_RECORDSET,
            $data['to'] ?? self::DEFAULT_LIMIT_RECORDSET,
            $data['orderby'] ?? self::DEFAULT_ORDER_RECORDSET,
            $data['price_from'] ?? null,
            $data['price_to'] ?? null,
            $data['category'] ?? null,
            $data['allow_load'] ?? null,
            $data['allow_hideaway'] ?? null,
            $data['allow_no_license'] ?? null,
            $data['contract_type'] ?? self::DEFAULT_CONTRACT_TYPE
        );
    }

    /**
     * Private constructor, command shuldn't be modified
     */
    private function __construct(
        int $from,
        int $to,
        string $order_by_field,
        ?float $price_from,
        ?float $price_to,
        ?int $category,
        ?int $allow_load,
        ?int $allow_hideaway,
        ?int $allow_no_license,
        int $contract_type
    ) {

        $this->from_record = $from;
        $this->to_record = $to;
        $this->order_by_field = $this->handleRecordSetOrder($order_by_field);
        $this->price_from = $price_from;
        $this->price_to = $price_to;
        $this->category_id = $category;
        $this->allow_load = $allow_load;
        $this->allow_hideaway = $allow_hideaway;
        $this->allow_no_license = $allow_no_license;
        $this->contract_type = $contract_type;
    }

    public function getFrom(): int
    {
        return $this->from_record;
    }

    public function getTo(): int
    {
        return $this->to_record;
    }

    public function getFromPrice(): ?float
    {
        return $this->price_from;
    }

    public function getToPrice(): ?float
    {
        return $this->price_to;
    }

    public function getCategoryId(): ?int
    {
        return $this->category_id;
    }

    public function getAll(): array
    {
        return [
            'filters' => [
                'price_from' => $this->price_from,
                'price_to' => $this->price_to,
                'category' => $this->category_id,
                'allow_load' => $this->allow_load,
                'allow_hideaway' => $this->allow_hideaway,
                'allow_no_license' => $this->allow_no_license,
                'contract_type' => $this->contract_type
            ],
            'limits' => [
                'from' => $this->from_record,
                'to' => $this->to_record,
            ],
            'order' => $this->order_by_field
        ];
    }

    private function handleRecordSetOrder(string $recordset_order): array
    {
        list($field, $dir) = explode('_', $recordset_order);
        return [
            'field' => $field,
            'dir'   => $dir
        ];
    }
}
