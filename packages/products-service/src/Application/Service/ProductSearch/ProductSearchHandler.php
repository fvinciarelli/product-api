<?php

namespace VinciarelliFranco\ProductService\Application\Service\ProductSearch;

use Exception;
use GuzzleHttp\Handler\Proxy;
use Illuminate\Database\Eloquent\Builder;
use VinciarelliFranco\ProductService\Domain\Product;
use Symfony\Component\HttpKernel\Exception\HttpException;
use VinciarelliFranco\ProductService\Share\IDDDCommandHandlers;

class ProductSearchHandler implements IDDDCommandHandlers
{
    public static function handle($command)
    {
        try {
            return Product::search($command->getAll());
        } catch (Exception $e) {
            //Log an send error
            throw new HttpException(500, $e->getMessage());
        }
    }
}
