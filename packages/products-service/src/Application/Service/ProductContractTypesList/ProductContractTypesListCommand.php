<?php

namespace VinciarelliFranco\ProductService\Application\Service\ProductContractTypesList;

use VinciarelliFranco\ProductService\Share\IDDDCommand;

/**
 * Product Contract Type Command. 
 */
class ProductContractTypesListCommand implements IDDDCommand
{
    public static function fromWebRequest(?array $data): ProductContractTypesListCommand
    {
        return new self();
    }

    private function __construct()
    {
    }

    public function getAll(): array
    {
        return [];
    }
}
