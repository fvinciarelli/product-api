<?php

namespace VinciarelliFranco\ProductService\Application\Service\ProductContractTypesList;

use Exception;
use Symfony\Component\HttpKernel\Exception\HttpException;
use VinciarelliFranco\ProductService\Domain\ProductContract;
use VinciarelliFranco\ProductService\Share\IDDDCommandHandlers;

/**
 * Product Contract Handler
 */
class ProductContractTypesListHandler implements IDDDCommandHandlers
{
    /**
     * Handle command transfered by command bus.
     * 
     */
    public static function handle($command)
    {
        try {
            return ProductContract::distinct()->orderBy('months')->get(['months']);
        } catch (Exception $e) {
            //Log an send error
            throw new HttpException(500, $e->getMessage());
        }
    }
}
