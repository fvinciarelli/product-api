<?php

namespace VinciarelliFranco\ProductService\Application\Service\ProductPrinceRangeList;

use Exception;
use VinciarelliFranco\ProductService\Domain\ProductContract;
use VinciarelliFranco\ProductService\Share\IDDDCommandHandlers;

class ProductPrinceRangeListHandler implements IDDDCommandHandlers
{
    public static function handle($command)
    {
        try {
            $min = ProductContract::where('months', '=', 12)->min('price');
            $max = ProductContract::where('months', '=', 12)->max('price');
            $count =  ProductContract::where('months', '=', 12)->count();
            return [
                'min' => $min,
                'max' => $max,
                'count' => $count
            ];
        } catch (Exception $e) {
            //Log an send error
            return [];
        }
    }
}
