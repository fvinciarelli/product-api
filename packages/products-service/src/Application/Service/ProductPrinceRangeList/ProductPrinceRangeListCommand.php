<?php

namespace VinciarelliFranco\ProductService\Application\Service\ProductPrinceRangeList;

use Exception;
use VinciarelliFranco\ProductService\Share\IDDDCommand;

class ProductPrinceRangeListCommand implements IDDDCommand
{
    public static function fromWebRequest(?array $data): ProductPrinceRangeListCommand
    {
        return new self();
    }

    private function __construct()
    {
    }

    public function getAll(): array
    {
        return [];
    }
}
