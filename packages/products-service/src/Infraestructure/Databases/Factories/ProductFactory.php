<?php

namespace VinciarelliFranco\ProductService\Infraestructure\Databases\Factories;

use Faker\Factory as Faker;
use Illuminate\Database\Eloquent\Factories\Factory;
use VinciarelliFranco\ProductService\Domain\Product;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $faker = Faker::create();

        return [
            'title' => $faker->words(5, true),
            'description' => $faker->words(30, true),
            'image' => $faker->image(null, 640, 480),
            'autonomy' => $faker->randomNumber(3, true),
            'allow_hideaway' => $faker->numberBetween(0, 1),
            'allow_load' => $faker->numberBetween(0, 1),
            'allow_no_license' => $faker->numberBetween(0, 1),
            'active' => 1
        ];
    }
}
