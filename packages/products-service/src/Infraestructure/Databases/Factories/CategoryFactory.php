<?php

namespace VinciarelliFranco\ProductService\Infraestructure\Databases\Factories;

use Faker\Factory as Faker;
use Illuminate\Database\Eloquent\Factories\Factory;
use VinciarelliFranco\ProductService\Domain\Category;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class CategoryFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Category::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $faker = Faker::create();

        return [
            'title' => $faker->words(5, true),
            'description' => $faker->words(30, true),
            'active' => 1
        ];
    }
}
