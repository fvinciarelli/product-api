<?php

namespace VinciarelliFranco\ProductService\Infraestructure\Databases\Factories;

use Faker\Factory as Faker;
use Illuminate\Database\Eloquent\Factories\Factory;
use VinciarelliFranco\ProductService\Domain\ProductRating;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class ProductRatingFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ProductRating::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $faker = Faker::create();

        return [
            'rate' => $faker->randomElement([1, 2, 3, 4, 5]),
            'comment' => $faker->words(30, true)
        ];
    }
}
