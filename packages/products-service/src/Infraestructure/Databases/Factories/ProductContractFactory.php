<?php

namespace VinciarelliFranco\ProductService\Infraestructure\Databases\Factories;

use Faker\Factory as Faker;
use Illuminate\Database\Eloquent\Factories\Factory;
use VinciarelliFranco\ProductService\Domain\ProductContract;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class ProductContractFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ProductContract::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $faker = Faker::create();

        return [
            'months' => $faker->randomElement([12, 24, 36, 48]),
            'price' => $faker->randomFloat(2, 10, 500)
        ];
    }
}
