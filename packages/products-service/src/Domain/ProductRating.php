<?php

namespace VinciarelliFranco\ProductService\Domain;

use Illuminate\Database\Eloquent\Model;
use VinciarelliFranco\ProductService\Domain\Product;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use VinciarelliFranco\ProductService\Infraestructure\Databases\Factories\ProductRatingFactory;

class ProductRating extends Model
{
    use HasFactory;

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

    protected static function newFactory()
    {
        return ProductRatingFactory::new();
    }
}
