<?php

namespace VinciarelliFranco\ProductService\Domain;

use Illuminate\Database\Eloquent\Builder;

trait ProductSearch
{
    static function search($data)
    {
        $query = self::applyfilters(self::query(), $data['filters']);
        $query = self::applyLimits($query, $data['limits']);
        $query = self::applyOrder($query, $data['order']);
        return self::getResults($query->with(['ratings']));
    }

    static function applyFilters(Builder $query, array $filters)
    {
        foreach ($filters as $filterClass => $value) {
            $filter = self::createFilterDecorator($filterClass);

            if (self::isValidDecorator($filter)) {
                $query = $filter::apply($query, $value);
            }
        }
        return $query;
    }

    static function createFilterDecorator($decorator)
    {
        return __namespace__ . '\\Filters\\' . str_replace(' ', '', ucwords(str_replace('_', ' ', $decorator)));
    }

    static function isValidDecorator($decorator)
    {
        return class_exists($decorator);
    }

    static function applyLimits(Builder $query, array $limits)
    {
        return $query->skip($limits['from'])->take($limits['to']);
    }

    static function applyOrder(Builder $query, array $recordset_order)
    {
        return $query->orderBy($recordset_order['field'], $recordset_order['dir']);
    }

    static function getResults(Builder $query)
    {
        return $query->get();
    }
}
