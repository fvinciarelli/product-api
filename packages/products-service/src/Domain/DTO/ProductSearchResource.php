<?php

namespace VinciarelliFranco\ProductService\Domain\DTO;

use Illuminate\Http\Resources\Json\JsonResource;

//https://dev.to/dalelantowork/laravel-8-api-resources-for-beginners-2cpa
class ProductSearchResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'price' => $this->price,
            'description' => $this->description,
            'image' => $this->image,
            'rating' => $this->ratings->avg('rate'),
            'total_rating' => $this->ratings->count(),
        ];
    }
}
