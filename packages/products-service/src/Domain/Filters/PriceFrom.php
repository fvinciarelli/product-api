<?php

namespace VinciarelliFranco\ProductService\Domain\Filters;

use Illuminate\Database\Eloquent\Builder;

class PriceFrom
{
    public static function apply(Builder $query, $value)
    {
        if (empty($value)) {
            return $query;
        }
        return $query->where('price', '>=', $value);
    }
}
