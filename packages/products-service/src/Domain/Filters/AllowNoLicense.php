<?php

namespace VinciarelliFranco\ProductService\Domain\Filters;

use Illuminate\Database\Eloquent\Builder;

class AllowNoLicense
{
    public static function apply(Builder $query, $value)
    {
        if (is_null($value)) {
            return $query;
        }
        return $query->where('allow_no_license', '=', $value);
    }
}
