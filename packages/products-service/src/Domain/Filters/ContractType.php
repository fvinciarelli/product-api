<?php

namespace VinciarelliFranco\ProductService\Domain\Filters;

use Illuminate\Database\Eloquent\Builder;

class ContractType
{
    public static function apply(Builder $query, $value)
    {
        if (empty($value)) {
            return $query;
        }
        
        return $query->join('product_contracts', 'product_contracts.product_id', '=', 'products.id')
        ->select('products.*', 'product_contracts.price as price')
        ->where('product_contracts.months', '=', $value);
    }
}
