<?php

namespace VinciarelliFranco\ProductService\Domain\Filters;

use Illuminate\Database\Eloquent\Builder;

class AllowHideaway
{
    public static function apply(Builder $query, $value)
    {
        if (is_null($value)) {
            return $query;
        }
        return $query->where('allow_hideaway', '=', $value);
    }
}
