<?php

namespace VinciarelliFranco\ProductService\Domain\Filters;

use Illuminate\Database\Eloquent\Builder;

class Category
{
    public static function apply(Builder $query, $value)
    {
        if (empty($value)) {
            return $query;
        }
        return $query->where('category_id', '=', $value);
    }
}
