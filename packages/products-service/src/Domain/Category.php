<?php

namespace VinciarelliFranco\ProductService\Domain;

use Illuminate\Database\Eloquent\Model;
use VinciarelliFranco\ProductService\Domain\Product;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use VinciarelliFranco\ProductService\Infraestructure\Databases\Factories\CategoryFactory;

class Category extends Model
{
    use HasFactory;

    public function products()
    {
        return $this->hasMany(Product::class, 'product_id');
    }

    protected static function newFactory()
    {
        return CategoryFactory::new();
    }
}
