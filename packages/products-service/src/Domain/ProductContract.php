<?php

namespace VinciarelliFranco\ProductService\Domain;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use VinciarelliFranco\ProductService\Infraestructure\Databases\Factories\ProductContractFactory;

class ProductContract extends Model
{
    use HasFactory;

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

    protected static function newFactory()
    {
        return ProductContractFactory::new();
    }
}
