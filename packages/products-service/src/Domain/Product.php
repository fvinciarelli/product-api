<?php

namespace VinciarelliFranco\ProductService\Domain;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use VinciarelliFranco\ProductService\Domain\ProductSearch;
use VinciarelliFranco\ProductService\Infraestructure\Databases\Factories\ProductFactory;

class Product extends Model
{
    use HasFactory;
    use ProductSearch;

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function ratings()
    {
        return $this->hasMany(ProductRating::class, 'product_id');
    }

    public function contracts()
    {
        return $this->hasMany(ProductContract::class, 'product_id');
    }

    protected static function newFactory()
    {
        return ProductFactory::new();
    }
}
