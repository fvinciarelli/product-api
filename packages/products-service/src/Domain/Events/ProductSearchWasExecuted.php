<?php

namespace VinciarelliFranco\ProductService\Domain\Events;

use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Request;
use Illuminate\Foundation\Events\Dispatchable;
/**
 * Event to signalice thar a product search was excecuted
 */
class ProductSearchWasExecuted
{
    use Dispatchable;
    use SerializesModels;

    /**
     * Request
     */
    public $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }
}
