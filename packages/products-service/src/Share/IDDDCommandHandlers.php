<?php

namespace VinciarelliFranco\ProductService\Share;

interface IDDDCommandHandlers
{
    public static function handle($command);
}
