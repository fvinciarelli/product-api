<?php

namespace VinciarelliFranco\ProductService\Providers;

use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;
use VinciarelliFranco\ProductService\Providers\Middlewares\AfterCacheSearchMiddleware;
use VinciarelliFranco\ProductService\Providers\Middlewares\BeforeCacheSearchMiddleware;

class ProductServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/../../config/config.php', 'productService');
    }

    public function boot()
    {
        $this->registerRoutes();
        $this->loadMigrationsFrom(__DIR__ . '/../Infraestructure/Databases/Migrations');
        
        $router = $this->app->make(Router::class);
        $router->aliasMiddleware('BeforeCacheSearchMiddleware', BeforeCacheSearchMiddleware::class);
        $router->aliasMiddleware('AfterCacheSearchMiddleware', AfterCacheSearchMiddleware::class);

        if ($this->app->runningInConsole()) {
            $this->publishSeeders();
        }
    }

    protected function registerRoutes()
    {
        Route::group($this->routeConfiguration(), function () {
            $this->loadRoutesFrom(__DIR__ . '/Routes/web.php');
        });
    }

    protected function routeConfiguration()
    {
        return [
            'prefix' => config('productService.prefix'),
            'middleware' => config('productService.middleware'),
        ];
    }

    protected function publishSeeders()
    {
        $this->publishes([
            __DIR__ . '/../Infraestructure/Databases/Seeds/ProductsServiceSeeder.php' => database_path('seeders/ProductsServiceSeeder.php'),
        ], 'product-service-seeds');
    }
}
