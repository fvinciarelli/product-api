<?php

use Illuminate\Support\Facades\Route;
use VinciarelliFranco\ProductService\Providers\Http\Controllers\ProductSearchController;
use VinciarelliFranco\ProductService\Providers\Http\Controllers\ProductFilterOrderListController;
use VinciarelliFranco\ProductService\Providers\Http\Controllers\ProductPrinceRangeListController;
use VinciarelliFranco\ProductService\Providers\Http\Controllers\ProductContractTypesListController;

Route::prefix('products')->group(function () {
    Route::get('/filters', ProductFilterOrderListController::class)->name('products.filter-order');
    Route::get('/contracts', ProductContractTypesListController::class)->name('products.product-contracts');
    Route::get('/price-range', ProductPrinceRangeListController::class)->name('products.price-range');
    Route::get('/', ProductSearchController::class)->middleware(['BeforeCacheSearchMiddleware','AfterCacheSearchMiddleware'])->name('products.search');
});
