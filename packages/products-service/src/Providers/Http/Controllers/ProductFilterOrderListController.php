<?php

namespace VinciarelliFranco\ProductService\Providers\Http\Controllers;

use Illuminate\Support\Facades\Request;
use VinciarelliFranco\ProductService\Application\Service\ProductFiltersList\ProductFiltersListCommand;
use VinciarelliFranco\ProductService\Application\Service\ProductFiltersList\ProductFiltersListHandler;

class ProductFilterOrderListController extends Controller
{
    /**
     * 1- Here we materialize adapter and port communication
     * 2- Web port adapt a request to a command that is handled by its associated service
     */
    public function __invoke(Request $request)
    {
        $command = ProductFiltersListCommand::fromWebRequest($request::all());
        $result = ProductFiltersListHandler::handle($command);
        //Data must always by returned adapted bease on port requirements
        return response()->json($result);
    }
}
