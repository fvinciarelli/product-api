<?php

namespace VinciarelliFranco\ProductService\Providers\Http\Controllers;

use Illuminate\Support\Facades\Request;
use VinciarelliFranco\ProductService\Domain\DTO\ProductPriceRangeResource;
use VinciarelliFranco\ProductService\Application\Service\ProductPrinceRangeList\ProductPrinceRangeListCommand;
use VinciarelliFranco\ProductService\Application\Service\ProductPrinceRangeList\ProductPrinceRangeListHandler;

class ProductPrinceRangeListController extends Controller
{
    /**
     * 1- Here we materialize adapter and port communication
     * 2- Web port adapt a request to a command that is handled by its associated service
     */
    public function __invoke(Request $request)
    {
        $command = ProductPrinceRangeListCommand::fromWebRequest($request::all());
        $result = ProductPrinceRangeListHandler::handle($command);
        //Data must always by returned adapted bease on port requirements
        return new ProductPriceRangeResource($result);
    }
}
