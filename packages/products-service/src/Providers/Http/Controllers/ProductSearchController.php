<?php

namespace VinciarelliFranco\ProductService\Providers\Http\Controllers;

use Illuminate\Support\Facades\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use VinciarelliFranco\ProductService\Domain\DTO\ProductSearchResource;
use VinciarelliFranco\ProductService\Domain\Events\ProductSearchWasExecuted;
use VinciarelliFranco\ProductService\Application\Service\ProductSearch\ProductSearchCommand;
use VinciarelliFranco\ProductService\Application\Service\ProductSearch\ProductSearchHandler;

class ProductSearchController extends Controller
{
    /**
     * 1- Here we materialize adapter and port communication
     * 2- Web port adapt a request to a command that is handled by its associated service
     */
    public function __invoke(Request $request)
    {
        try {
            $command = ProductSearchCommand::fromWebRequest($request::all());
            $result = ProductSearchHandler::handle($command);

            //It is always a good practice to signalice that a task has finished
            event(new ProductSearchWasExecuted($request));

            //Data must always by returned adapted bease on port requirements
            return ProductSearchResource::collection($result);
        } catch (\Exception $e) {
            throw new HttpException(500, $e->getMessage());
        }
    }
}
