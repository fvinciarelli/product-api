<?php

namespace VinciarelliFranco\ProductService\Providers\Http\Controllers;

use Illuminate\Support\Facades\Request;
use VinciarelliFranco\ProductService\Domain\DTO\ProductContractTypeResource;
use VinciarelliFranco\ProductService\Application\Service\ProductContractTypesList\ProductContractTypesListCommand;
use VinciarelliFranco\ProductService\Application\Service\ProductContractTypesList\ProductContractTypesListHandler;

class ProductContractTypesListController extends Controller
{
    public function __invoke(Request $request)
    {
        $command = ProductContractTypesListCommand::fromWebRequest($request::all());
        $result = ProductContractTypesListHandler::handle($command);
        return new ProductContractTypeResource($result);
    }
}
