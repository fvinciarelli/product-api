<?php

namespace VinciarelliFranco\ProductService\Providers\Middlewares;

use Closure;
use Illuminate\Support\Facades\Redis;

class BeforeCacheSearchMiddleware
{
    public function handle($request, Closure $next)
    {
        //Check if request parameters combination have been
        //already returned by the system for a given period of time
        $key = hash('md5', $request->fullUrl());
        $data = Redis::get($key);

        if (!is_null($data)) {
            return response($data);
        }

        return $next($request);
    }
}
