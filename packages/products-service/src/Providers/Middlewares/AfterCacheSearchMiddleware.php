<?php

namespace VinciarelliFranco\ProductService\Providers\Middlewares;

use Closure;
use Illuminate\Support\Facades\Redis;

class AfterCacheSearchMiddleware
{
    public function handle($request, Closure $next)
    {
        //Save request url to construct redis key
        $key = hash('md5', $request->fullUrl());

        //Call the API
        $response = $next($request);

        //Check if request parameters combination have been
        //saved before or not
        if (!Redis::get($key)) { //If not save it
            Redis::set($key, $response->getContent(), 'EX', 30);
        }

        return $response;
    }
}
