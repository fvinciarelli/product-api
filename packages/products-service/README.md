<p align="center">
  <a href="https://codely.com">
    <img alt="Codely logo" src="https://elreferente.es/wp-content/uploads/2021/09/Captura-de-pantalla-2022-02-15-a-las-12.16.30.png" width="300px" height="92px"/>
  </a>
</p>

<h1 align="center">
  🐘🎯 Backend Test with Hexagonal Architecture and DDD in PHP by Franco Vinciarelli
</h1>


## 🚀 Environment Setup

### 🐳 Needed tools

1. [Install Docker and Docker-Compose](https://www.docker.com/get-started)
2. Clone this project: `git clone https://gitlab.com/fvinciarelli/product-api.git product-api-test`
3. Move to the project folder: `cd product-api-test`

### 🛠️ Environment configuration

1. Create the necessary Docker's images and build the required containers executing: `docker-compose up -d` at your terminal. 

If everything went ok you will have 4 servers running in your computer.

This servers are:

    * app-test   - A server php8-fpm server
    * db-test    - A Mysql server 
    * nginx-test - A Webserver based on Ngnix connected to fpm instance
    * redis-test - A Cache server based on Redis to speed up subsecuense calls to our API

You can check if server are running by inspecting the results of the following command: `sudo docker ps -a`

2. Connect to the <b>app</b> container executing `docker-compose exec app bash`
3. Install required components to the laravel project using composer. `composer install`
4. Create required configuration file copying from example config file. `cp .env.example .env`
5. Run Laravel required commands:

    5.1 `php artisan key:generate`

    5.2 `php migrate`

6. Our solution is implemented as a laravel's package so to have some example data to test the development you should publish the project's seeder and seed data to the database. 

    6.1 `php artisan vendor:publish --tag=product-service-seeds`

    6.2 `php artisan db:seed --class=ProductsServiceSeeder`

### 🔥 Application execution

1. Start project's ecosystem with Docker executing: `docker-compose start` at project's root directory.
2. Then you'll have 1 app available wich provides 4 API:
   1. Product Service search - http://localhost:8080/api/v1/products/ .The following options are available to further refine products list. Each option should be added to the API query's URL.

        * 'from' => Number. Used to paginate results. Represents the starting number from wich records will be taken from a the set of record obtained by a query. If not present 0 (first) is used. 
        *  'to' => Number. Used to paginate results. Represents the amount of records to be send from the original recordset obtained by a query. If not present 10 is used, meaning that 10 registers will be returned if available. 
        *  'price_from' => Number. Used to filter products by its contract's price. If present it is used as a starting price of a product's contract. 
        *  'price_to' => Number. Used to filter products by its contract's price. If present it is used as the maximum price for a contract. 
        *  'category' => Number. Used to filter products by product's category.
        *  'allow_load' => 1 or 0. Used to filter products based on its possibility to carry weights.
        *  'allow_hideaway' => 1 or 0. Used to filter products based on its possibility to be used at the highway.
        *  'allow_no_license' => 1 or 0. Used to filter products based on its possibility to be used without having a driving license.
        *  'contract_type' => Number. Used to filter product by a number of months product's associate contract should have. If no present 12 is asumed. 
        *  'orderby' => string. Used to order recordset base on a particular product's feature. Also define if order shuld be in Ascending or Descending order.

   2. Price Range Data EndPoint to construct product's price filter API at frontend http://localhost:8080/api/v1/products/price-range
   3. Product renting contract's lenght API to consume contract type options. http://localhost:8080/api/v1/products/contracts
   4. Product's ordering options API. http://localhost:8080/api/v1/products/filters

### ✅ Tests code quality and execution

The project have been developed using several code's quality checking tools in order to ensure solution's quality. Code is distibuted as a package, so to be able to run the commands you should move to **products-service** package which is inside **packages** directory in project's root folder. 
IMPORTANT: You MUST be inside 'app' container to run this commands. (`docker-compose exec app bash`)

1. To check codes adherence to PSR-12: `composer cs`
2. To if the development has code smells: `composer  code-smell-check`
3. Finally yo Execute PHPUnit tests: `cd ../.. && php artisan test`

## 👩‍💻 Project explanation

This project is structure as DDD's 🙂 requirements. 
 
```scala
src
   |-- 
|     |-- Application
|     |   |-- Service
|     |   |     |-- ProductSearch // A collection a services that the solution offer.
|           .....
|           .....
|     |-- Domain
|        `-- Product.php // A collection of App models
|            .....
|            .....
|     |-- Infrastructure // The infrastructure of our module
|            `-- Migrations // A collection of migration files
```

### 🎯 Hexagonal Architecture

This repository follows the Hexagonal Architecture pattern. External request's are trait by command which are handled by the associate application's services implementing a **bus command pattern**

### Models, active record and repository patterns

Here we are taking adventage of Laravel's Eloquent ORM which provides us with to handle serveral of the requirements to handel models.  

## 📱 Events

We belive that is a good practice to use event's to communicate when a service has conclude its tasks. He we have make available an event called **ProductSearchWasExecuted** that is emitted when a search has finish. This event can be used por example to log most frequents query's an then used it to index databases to get queries with better performance or to check id a particular filter is been used or not.

## 🤩 Extra

The package provides a **before** and **after** **middleware** that is applied to the  **product_search's** route in order to cache results for a giver set of filters in order to leverage system performance and hence the user's experience. The results are stored in Redis.

## 🤔 More Documentation

Please find at project's root directory the exported Postman's collection file to test current API. File is called  **PangaGO.postman_collection.json**

