<?php

namespace VinciarelliFranco\ProductService\Tests;

use VinciarelliFranco\ProductService\Providers\ProductServiceProvider;

class TestCase extends \Orchestra\Testbench\TestCase
{
    public function setUp(): void
    {
        parent::setUp();
        $this->loadLaravelMigrations(['--database' => 'testing']);
        $this->artisan('migrate', ['--database' => 'testing'])->run();
    }

    protected function getPackageProviders($app)
    {
        return [
          ProductServiceProvider::class,
        ];
    }

    protected function getEnvironmentSetUp($app)
    {
        // import the CreatePostsTable class from the migration
        include_once __DIR__ . '/../src/Infraestructure/Databases/Migrations/2022_12_04_220670_create_products_table.php';

        // run the up() method of that migration class
        //(new \CreateProductsTable)->up();
    }
}
