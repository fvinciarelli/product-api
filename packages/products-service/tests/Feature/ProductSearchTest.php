<?php

namespace VinciarelliFranco\ProductService\Tests\Feature;

use Illuminate\Http\Response;
use Illuminate\Foundation\Testing\RefreshDatabase;
use VinciarelliFranco\ProductService\Domain\Category;
use VinciarelliFranco\ProductService\Domain\Product;
use VinciarelliFranco\ProductService\Domain\ProductContract;
use VinciarelliFranco\ProductService\Domain\ProductRating;
use VinciarelliFranco\ProductService\Tests\TestCase;

class ProductSearchTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function check_product_search_route_is_working()
    {   
        $this->withoutExceptionHandling();
        $response = $this->call('GET', '/api/v1/products/');
        $response->assertStatus(200);
    }

    /** @test */
    public function check_product_contracts_route_is_working()
    {   
        $this->withoutExceptionHandling();
        $response = $this->call('GET', '/api/v1/products/contracts');
        $response->assertStatus(200);
    }

    /** @test */
    public function check_product_search_price_range_is_working()
    {   
        $this->withoutExceptionHandling();
        $response = $this->call('GET', '/api/v1/products/price-range');
        $response->assertStatus(200);
    }

    /** @test */
    public function check_product_search_with_contract_type()
    {   
        Category::factory(2)->create()->each(
            function ($category) {
                Product::factory(10)->make()->each(function ($prod) use ($category) {
                    $prod->category_id = $category->id;
                    $prod->save();

                    $ratings = ProductRating::factory(5)->make();
                    $prod->ratings()->saveMany($ratings);

                    $contract = ProductContract::factory()->make();
                    $prod->contracts()->save($contract);

                });
            }
        );

        $catgoria = Category::factory()->create();
        echo "Cantidad de categorias: " . Category::count()."\n";
        echo "Cantidad de Productos: ". Product::count()."\n";

        $response = $this->get('/api/v1/products/');
        //dd($response);
        
    }
}
