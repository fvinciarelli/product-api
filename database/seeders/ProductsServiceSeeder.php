<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use VinciarelliFranco\ProductService\Domain\Product;
use VinciarelliFranco\ProductService\Domain\Category;
use VinciarelliFranco\ProductService\Domain\ProductContract;
use VinciarelliFranco\ProductService\Domain\ProductRating;

class ProductsServiceSeeder extends Seeder
{
    public function run()
    {
        
        $cat1 = Category::factory()->create();
        $cat2 = Category::factory()->create();

        //Cat1 have 2 Products
        
        //Product 1
        //P1 have a have contract type for 12 and 24 months

        $p1 = Product::factory()->make();
        $p1->category_id = $cat1->id;
        $p1->save();
        
        $c1 = ProductContract::factory()->make();
        $c1->months = 12;
        $p1->contracts()->save($c1);

        $c2 = ProductContract::factory()->make();
        $c2->months = 24;
        $p1->contracts()->save($c2);

        $r1 = ProductRating::factory()->make();
        $r1->rate = 1;
        $p1->ratings()->save($r1);

        $r1 = ProductRating::factory()->make();
        $r1->rate = 1;
        $p1->ratings()->save($r1);
       
        
        //Product 2
        //P2 have a have contract type for 24 and 36 months

        $r2 = ProductRating::factory()->make();
        $r2->rate = 1;
        $p1->ratings()->save($r2);

        $p2 = Product::factory()->make();
        $p2->category_id = $cat1->id;
        $p2->save();
        
        $c3 = ProductContract::factory()->make();
        $c3->months = 24;
        $p2->contracts()->save($c3);

        $c4 = ProductContract::factory()->make();
        $c4->months = 36;
        $p2->contracts()->save($c4);

        $r3 = ProductRating::factory()->make();
        $r3->rate = 2;
        $p2->ratings()->save($r3);
       
        $r4 = ProductRating::factory()->make();
        $r4->rate = 2;
        $p2->ratings()->save($r4);

        
        //Cat2 have 3 Products

        //Product 3
        //P3 have contract type for 36 and 48 months
        $p3 = Product::factory()->make();
        $p3->category_id = $cat2->id;
        $p3->save();
        
        $c5 = ProductContract::factory()->make();
        $c5->months = 36;
        $p3->contracts()->save($c5);

        $c6 = ProductContract::factory()->make();
        $c6->months = 48;
        $p3->contracts()->save($c6);

        $r5 = ProductRating::factory()->make();
        $r5->rate = 3;
        $p3->ratings()->save($r5);
       
        $r6 = ProductRating::factory()->make();
        $r6->rate = 3;
        $p3->ratings()->save($r6);
        
        //Product 4
        //P4 have contract type for 12, 24, 36 and 48 months
        $p4 = Product::factory()->make();
        $p4->category_id = $cat2->id;
        $p4->save();
        
        $c5 = ProductContract::factory()->make();
        $c5->months = 12;
        $p4->contracts()->save($c5);

        $c6 = ProductContract::factory()->make();
        $c6->months = 24;
        $p4->contracts()->save($c6);

        $c7 = ProductContract::factory()->make();
        $c7->months = 36;
        $p4->contracts()->save($c7);

        $c8 = ProductContract::factory()->make();
        $c8->months = 48;
        $p4->contracts()->save($c8);

        $r7 = ProductRating::factory()->make();
        $r7->rate = 4;
        $p4->ratings()->save($r7);
       
        $r8 = ProductRating::factory()->make();
        $r8->rate = 4;
        $p4->ratings()->save($r8);


        //Product 5
        //P5 have contract type for 12 and 48 months

        $p5 = Product::factory()->make();
        $p5->category_id = $cat2->id;
        $p5->save();
        
        $c9 = ProductContract::factory()->make();
        $c9->months = 12;
        $p5->contracts()->save($c9);
        
        $c10 = ProductContract::factory()->make();
        $c10->months = 48;
        $p5->contracts()->save($c10);

        $r9 = ProductRating::factory()->make();
        $r9->rate = 4;
        $p5->ratings()->save($r9);
       
        $r10 = ProductRating::factory()->make();
        $r10->rate = 4;
        $p5->ratings()->save($r10);
        
    }
}
